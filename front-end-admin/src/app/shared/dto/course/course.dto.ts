import { BaseEntityDto } from '../base-entity.dto';

export class CourseDto extends BaseEntityDto<string>{
    public id: string;
    public thumbnailId: string;
    public name: string;
    public description: string;
}