﻿using System;
using Ft.Elearning.Admin.Notifications.Dto;

namespace Ft.Elearning.Admin.Notifications
{
    public interface INotificationAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateNotificationDto, GetNotificationForViewDto, GetNotificationForEditDto, GetNotificationInputDto>
    {

    }
}
