﻿using System;
using Ft.Elearning.Admin.Courses.Dto;

namespace Ft.Elearning.Admin.Courses
{
    public interface ICourseAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateCourseDto, GetCourseForViewDto, GetCourseForEditDto, GetCourseInputDto>
    {

    }
}
