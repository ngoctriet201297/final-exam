﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ft.Elearning.Admin.AssignLessons.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.AssignLessons
{
    public class AssignLessonAdminAppService : AppBaseAdminService<AssignLesson, Guid, CreateUpdateAssignLessonDto, GetAssignLessonForViewDto, GetAssignLessonForEditDto, GetAssignLessonInputDto>, IAssignLessonAdminAppService
    {
            private readonly IRepository<AssignLesson, Guid> _assignLessonRepository;
            private readonly IRepository<AssignLessonUser, Guid?> _assignLessonUserRepository;
            
        public AssignLessonAdminAppService(IRepository<AssignLesson, Guid> assignLessonRepository, IRepository<AssignLessonUser, Guid?> assignLessonUserRepository) : base(assignLessonRepository)
        {
            _assignLessonRepository = assignLessonRepository;
            _assignLessonUserRepository = assignLessonUserRepository;
        }

        public override Task<Guid> Save(CreateUpdateAssignLessonDto input)
        {
            var isAssignToAll = input.IsAssignToAll;
            var isManagerByAll = input.IsManageByAll;

            if (isAssignToAll)
            {
                input.AssignUsers = input.AssignUsers.Where(x => x.IsManager).ToList();
            }

            if (isManagerByAll)
            {
                input.AssignUsers = input.AssignUsers.Where(x => !x.IsManager).ToList();
            }

            return base.Save(input);
        }

        public override async Task Delete(Guid id)
        {
            var assignLesson = await _assignLessonRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (assignLesson == null)
            {
                throw new Exception("Item not found");
            }
            
            await _assignLessonUserRepository.DeleteAsync(x => x.AssignLessonId == assignLesson.Id);
            await _assignLessonRepository.DeleteAsync(assignLesson);
            await CurrentUnitOfWork.SaveChangesAsync();
        }
    }
}
