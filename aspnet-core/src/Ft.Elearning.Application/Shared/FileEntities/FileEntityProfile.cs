﻿using AutoMapper;
using Ft.Elearning.Entities;
using Ft.Elearning.Shared.FileEntities.Dto;

namespace Ft.Elearning.Shared.FileEntities
{
    public class FileEntityProfile : Profile
    {
        public FileEntityProfile()
        {
            CreateMap<CreateUpdateFileEntityDto, FileEntity>();
            CreateMap<FileEntity, FileEntityDto>();
        }
    }
}