export class ProductDocumentDto {
    public name: string;
    public logoImageUrl: string;
    public description: string;
    public content: string;
    public pdfUrl: string;
    public videoUrl: string;
    public imageUrl: string;
}