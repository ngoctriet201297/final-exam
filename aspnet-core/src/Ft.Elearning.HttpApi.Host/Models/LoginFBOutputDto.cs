﻿using System;
namespace Ft.Elearning.Models
{
    public class LoginFBOutputDto
    {
        public string access_token { get; set; }
        public long expires_in { get; set; }
    }
}
