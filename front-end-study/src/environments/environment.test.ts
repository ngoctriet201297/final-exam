import { Config } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'DanoneElearning',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://test-elearning-danone-api-advn.fractal.vn',
    redirectUri: baseUrl,
    clientId: 'DanoneElearning_App',
    responseType: 'password',
    scope: 'address email openid phone profile role',
    dummyClientSecret: '1q2w3e*',
    tokenEndpoint: 'https://test-elearning-danone-api-advn.fractal.vn/connect/token'
  },
  apis: {
    default: {
      url: 'https://test-elearning-danone-api-advn.fractal.vn',
      rootNamespace: 'Ft.DanoneElearning',
    },
  },
} as Config.Environment;
