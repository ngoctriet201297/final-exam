import { Injector } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { IOptionPopup, PopupComponent } from './components/popup-component/popup.component';
import { SpinnerService } from './service/spinner/spinner.service';

const API = environment.apis.default.url;
export class AppBaseComponent {

    private readonly route: ActivatedRoute;
    private readonly router: Router;
    private readonly toastService: ToastrService;
    private readonly matDialog: MatDialog;
    private readonly spinnerService: SpinnerService;

    constructor(protected readonly injector?: Injector) {
        this.route = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        this.toastService = injector.get(ToastrService);
        this.matDialog = injector.get(MatDialog);
        this.spinnerService = injector.get(SpinnerService);
    }

    redirectTo(url: string) {
        this.router.navigate([url]);
    }

    getMediaById(id: string) {
        if (id) {
            return API + "/api/app/file/" + id;
        }
        return "";
    }

    getParam(param: string): string {
        return this.route.snapshot.paramMap.get(param);
    }
    get tokenInfo(): { token: string, token_expire: string } {
        return {
            token: localStorage.getItem("token"),
            token_expire: localStorage.getItem("token_expire")
        }
    }

    get username(): string {
        return localStorage.getItem("user_fullname");
    }

    clearLocalStorage() {
        localStorage.clear();
    }

    showSuccess(message: string){
        this.toastService.success(message);
    }
    showError(message: string){
        this.toastService.error(message);
    }
    showWarning(message: string){
        this.toastService.warning(message);
    }

    openPopup(option: IOptionPopup){
        this.matDialog.open(PopupComponent, {
            width: "300px",
            data: option
        })
    }

    setBusy(){
        this.spinnerService.isProcessing = true;
    }
    clearBusy(){
        this.spinnerService.isProcessing = false;
    }
}