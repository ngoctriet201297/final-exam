﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Admin.Answers.Dto;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Questions.Dto
{
    public class CreateUpdateQuestionDto : BaseEntityDto<Guid?>
    {
        [Required]
        [MaxLength(QuestionConsts.MaxLengthContent)]
        public string Content { get; set; }
        public Guid? LessonId { get; set; }
        public List<CreateUpdateAnswerDto> Answers { get; set; }
    }
}
