﻿namespace Ft.Elearning.Consts
{
    public class TestConsts
    {
        public const int MaxLengthName = 255;
        public const int MaxLengthTitle = 255;
        public const int MaxLengthDescription = 5000;
    }
}