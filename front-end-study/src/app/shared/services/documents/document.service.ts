import { Injectable } from '@angular/core';
import {RestService, PagedResultDto} from "@abp/ng.core";
import { GetProductDocumentInputDto } from '../../dto/get-product-document-input.dto';
import { ProductDocumentDto } from '../../dto/product-document.dto';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DocumentService {

    public readonly API = "/api/app/product-document-public/";
    public readonly apiName = "Default";
    constructor(private readonly restService: RestService){

    }

    getAll(input: GetProductDocumentInputDto): Observable<PagedResultDto<ProductDocumentDto>>{
        return this.restService.request(
            { method: "GET", url: this.API + "by-paged", params: input },
            { apiName: this.apiName }
        )
    }

}