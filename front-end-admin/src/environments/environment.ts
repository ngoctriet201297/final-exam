import { Config } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'DanoneElearning',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'http://localhost:44385',
    redirectUri: baseUrl,
    clientId: 'Elearning_App',
    responseType: 'password',
    scope: 'openid',
    dummyClientSecret: '1q2w3e*',
    tokenEndpoint: 'http://localhost:44385'
  },
  apis: {
    default: {
      url: 'http://localhost:44385',
      rootNamespace: 'Ft.DanoneElearning',
    },
  },
} as Config.Environment;
