﻿using AutoMapper;
using Ft.Elearning.Admin.Questions.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Questions
{
    public class QuestionAdminProfile : Profile
    {
        public QuestionAdminProfile()
        {
            CreateMap<CreateUpdateQuestionDto, Question>();
            CreateMap<Question, GetQuestionForViewDto>();
            CreateMap<Question, GetQuestionForEditDto>();
        }
    }
}
