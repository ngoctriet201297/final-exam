﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ft.Elearning.Entities;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using System.Linq.Dynamic.Core;
using Ft.Elearning.Enums;

namespace Ft.Elearning.Admin
{
    public abstract class AppBaseAdminService<TEntity, TPrimaryKey, TCreateUpdate, TViewOutput, TEditOutput, TGetAllInput> : ElearningAppService,
        IAppBaseAdminService<TPrimaryKey, TCreateUpdate, TViewOutput, TEditOutput, TGetAllInput>
        where TPrimaryKey : struct
        where TEntity : BaseEntity<TPrimaryKey>
        where TCreateUpdate : BaseEntityDto<TPrimaryKey?>
        where TViewOutput : BaseEntityDto<TPrimaryKey>
        where TEditOutput : BaseEntityDto<TPrimaryKey?>
        where TGetAllInput : PagedAndSortedResultRequestDto
    {
        protected readonly IRepository<TEntity, TPrimaryKey> Repository;

        #region Check Policy
        protected virtual async Task CheckGetPolicyAsync()
        {
            return;
        }

        protected virtual async Task CheckGetListPolicyAsync()
        {
            return;
        }

        protected virtual async Task CheckCreatePolicyAsync()
        {
            return;
        }

        protected virtual async Task CheckUpdatePolicyAsync()
        {
            return;
        }

        protected virtual async Task CheckDeletePolicy()
        {
            return;
        }
        #endregion

        #region Filter Function
        protected virtual IQueryable<TEntity> CreateFilteredQuery(TGetAllInput input)
        {
            return Repository;
        }

        protected virtual IQueryable<TEntity> CreateFilteredQueryGetAll()
        {
            return Repository;
        }
        #endregion

        #region Mapper
        protected virtual TViewOutput MapToViewOutput(TEntity entity)
        {
            return ObjectMapper.Map<TEntity, TViewOutput>(entity);
        }

        protected virtual TEditOutput MapToEditOutput(TEntity input)
        {
            return ObjectMapper.Map<TEntity, TEditOutput>(input);
        }

        protected virtual TEntity MapToEntity(TCreateUpdate input)
        {
            return ObjectMapper.Map<TCreateUpdate, TEntity>(input);
        }

        protected virtual TEntity MapToEntity(TCreateUpdate input, TEntity entity)
        {
            return ObjectMapper.Map(input,entity);
        }

        #endregion

        #region Sorting and Paging
        protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> query, TGetAllInput input)
        {
            if (input is ISortedResultRequest sortInput)
            {
                if (!sortInput.Sorting.IsNullOrWhiteSpace())
                {
                    return query.OrderBy(sortInput.Sorting);
                }
            }
            return query.OrderByDescending(x => x.Id);
        }

        protected virtual IQueryable<TEntity> ApplyPaging(IQueryable<TEntity> query, TGetAllInput input)
        {
            if (input is IPagedResultRequest pagedResultRequest)
            {
                return query.PageBy(pagedResultRequest);
            }
            return query;
        }
        #endregion

        public AppBaseAdminService(IRepository<TEntity, TPrimaryKey> repository)
        {
            Repository = repository;
        }

        public virtual async Task Delete(TPrimaryKey id)
        {
            await CheckDeletePolicy();
            await Repository.DeleteAsync(id);
        }

        public virtual async Task<TEditOutput> Get(TPrimaryKey id)
        {
            await CheckGetPolicyAsync();

            var result = await Repository.GetAsync(id);

            return result == null ? null : MapToEditOutput(result);

        }

        public virtual async Task<List<TViewOutput>> GetAll()
        {
            await CheckGetListPolicyAsync();

            var query = CreateFilteredQueryGetAll();
            var result = await AsyncExecuter.ToListAsync(query);

            return result.Select(MapToViewOutput).ToList();
        }

        public virtual async Task<PagedResultDto<TViewOutput>> GetListPaged(TGetAllInput input)
        {
            await CheckGetListPolicyAsync();

            var query = CreateFilteredQuery(input);

            var totalCount = await AsyncExecuter.CountAsync(query);

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var result = await AsyncExecuter.ToListAsync(query);

            return new PagedResultDto<TViewOutput>(totalCount, result.Select(MapToViewOutput).ToList());
        }

        public virtual async Task<TPrimaryKey> Save(TCreateUpdate input)
        {
            if(input.Id.HasValue)
            {
                return await Update(input);
            }
            else
            {
                return await Create(input);
            }
        }

        protected virtual async Task<TPrimaryKey> Update(TCreateUpdate input)
        {
            await CheckUpdatePolicyAsync();

            var entity = await Repository.GetAsync(input.Id.Value);

            MapToEntity(input, entity);

            return input.Id.Value;
        }

        protected virtual async Task<TPrimaryKey> Create(TCreateUpdate input)
        {
            await CheckCreatePolicyAsync();

            var entity = MapToEntity(input);

            await Repository.InsertAsync(entity);

            await CurrentUnitOfWork.SaveChangesAsync();

            return entity.Id;
        }

        public virtual async Task ToggleStatus(TPrimaryKey id)
        {
            var entity = await Repository.GetAsync(id);
            entity.Status = entity.Status == Status.Active ? Status.InActive : Status.Active;
        }
    }
}
