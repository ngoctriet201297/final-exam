﻿using System;

namespace Ft.Elearning.Admin.Answers.Dto
{
    public class AnswerDto : BaseEntityDto
    {
        public string Content { get; set; }
        public bool IsRightAnswer { get; set; }
        public Guid QuestionId { get; set; }
    }
}
