import { ListResultDto, PagedResultDto } from "@abp/ng.core";
import { AfterViewInit, Component, Injector, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { merge } from "rxjs";
import { catchError, map, startWith, switchMap } from "rxjs/operators";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { GetProductDocumentForViewDto, ProductDocumentDto } from "src/app/shared/dto/product-document";
import { ProductDocumentService } from "src/app/shared/service/product-document/product-document.service";

@Component({
    templateUrl: "./product-document.component.html"
})
export class ProductDocumentComponent extends AppBaseComponent implements OnInit, AfterViewInit {

    columnsToDisplay = ["name", "description", "action"]
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource: PagedResultDto<GetProductDocumentForViewDto>;

    constructor(protected readonly injector: Injector,
        private readonly productDocumentService: ProductDocumentService) {
        super(injector);

    }
    ngAfterViewInit(): void {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page).pipe(
            startWith({}),
            switchMap(() => {
                this.setBusy();
                return this.getDataForTable();
            })
        ).subscribe(result => {
            this.clearBusy();
            this.dataSource = result;
        })
    }
    ngOnInit(): void {

    }
    create() {
        this.redirectTo("/admin/product-document/create");
    }
    edit(id: string) {
        this.redirectTo("/admin/product-document/edit/" + id);
    }
    getDataForTable() {
        return this.productDocumentService.getByPaged({
            maxResultCount: this.paginator.pageSize,
            skipCount: this.paginator.pageIndex * this.paginator.pageSize,
            sorting: this.sort.active ? (this.sort.active + " " + this.sort.direction) : ""
        });
    }
    openDialogForDelete(document: ProductDocumentDto){
        if(document){
            this.openPopup({
                title: "Cảnh báo",
                message: `Bạn có chắc chắn xoá ${document.name}`,
                type: "Warning",
                btnOk: {
                    callBack: () => {
                        this.setBusy();
                        this.productDocumentService.delete(document.id).subscribe(() => {
                            this.showSuccess("Bạn xoá dữ liệu thành công");
                            this.getDataForTable().subscribe(result => {
                                this.clearBusy();
                                this.dataSource = result;
                            });
                        }, () => this.showError("Bạn xoá dữ liệu thất bại"))
                    }
                },
                btnClose: {

                }
            })
        }
       
    }
}