import { Component, Injector, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { forkJoin } from "rxjs";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { ApiUserDistributor } from "src/app/shared/dto/api-user/api-user-distributor";
import { CourseDto } from "src/app/shared/dto/course";
import { CourseService } from "src/app/shared/service/courses/course.service";
import { LessonService } from "src/app/shared/service/courses/lesson.service";
import { UserService } from "src/app/shared/service/user/user-service";

@Component({
    templateUrl: "./create-edit-lesson.component.html"
})
export class CreateEditLessonComponent extends AppBaseComponent implements OnInit{
    lessonForm: FormGroup;
    distributors: ApiUserDistributor[];
    courses: CourseDto[];
    get lessonId(): string{
        return this.getParam("id");
    }

    constructor(protected readonly injector: Injector,
                private readonly fb: FormBuilder,
                private readonly courseService: CourseService,
                private readonly lessonService: LessonService,
                private readonly userService: UserService){
        super(injector)

        this.lessonForm = fb.group({
            name: ["",[Validators.required, Validators.maxLength(255)]],
            thumbnailId: [""],
            description: [""],
            content: [""],
            distributorId: ["",[Validators.required]],
            courseId: ["",[Validators.required]],
        });
    }

    get name(){
        return this.lessonForm.get("name");
    }

    ngOnInit(): void {
        this.getInitData();
        if(this.lessonId){
            this.lessonService.get(this.lessonId).subscribe((result) => {
                this.lessonForm.patchValue({
                    ...result
                })
            })
        }
    }

    save(){
        if(this.lessonForm.valid){
            this.setBusy();
            const body = this.lessonId ? {...this.lessonForm.value, ...{ id: this.lessonId } } : this.lessonForm.value;
            this.lessonService.save(body).subscribe(() => {
                this.showSuccess("Bạn đã cập nhật thành công");
                this.goBack();
            },() => {
                this.showError("Cập nhật thất bại");
            })
        }
    }

    getInitData(){
        // forkJoin([this.userService.getDistributor(), this.courseService.getAll()]).subscribe(([distributors, courses]) => {
        //     this.distributors = distributors.data;
        //     this.courses = courses;
        // })
    }

    goBack(){
        this.redirectTo("/admin/courses/lesson")
    }
}