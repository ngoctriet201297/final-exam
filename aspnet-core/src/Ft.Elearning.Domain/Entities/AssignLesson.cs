﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ft.Elearning.Entities
{
    public class AssignLesson : BaseEntity
    {
        public Guid LessonId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        
        [ForeignKey("LessonId")]
        public Lesson Lesson { get; set; }
        
        public ICollection<AssignLessonUser> AssignLessonUser { get; set; }
    }
}