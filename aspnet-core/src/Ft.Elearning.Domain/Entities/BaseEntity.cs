﻿using System;
using Ft.Elearning.Enums;
using Volo.Abp.Domain.Entities.Auditing;

namespace Ft.Elearning.Entities
{
    public class BaseEntity<T> : AuditedAggregateRoot<T>
    {
        public Status Status { get; set; }
        public int DistributorId { get; set; }
    }

    public class BaseEntity : BaseEntity<Guid>
    {

    }
}
