import { Component, Injector } from '@angular/core';
import { AppBaseComponent } from 'src/app/shared/app-base-component';

@Component({
  templateUrl: './document.component.html',
})
export class DocumentComponent extends AppBaseComponent {

  constructor(protected readonly injector: Injector) {
    super(injector);
  }

}
