﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ft.Elearning.Admin.Notifications.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Notifications
{
    public class NotificationAdminAppService : AppBaseAdminService<Notification, Guid, CreateUpdateNotificationDto, GetNotificationForViewDto, GetNotificationForEditDto, GetNotificationInputDto>, INotificationAdminAppService
    {
        private readonly IRepository<Notification, Guid> _notificationRepository;
        private readonly IRepository<NotificationUser, Guid?> _notificationUserRepository;
        public NotificationAdminAppService(IRepository<Notification, Guid> notificationRepository, IRepository<NotificationUser, Guid?> notificationUserRepository) : base(notificationRepository)
        {
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
        }

        protected override async Task<Guid> Update(CreateUpdateNotificationDto input)
        {
            try
            {
                var notification = await _notificationRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
                if (notification == null)
                    throw new Exception("Item not found");
                
                notification.Name = input.Name;
                notification.Content = input.Content;
                
                var listUsers = _notificationUserRepository.Where(x => x.NotificationId == input.Id).ToList();
                var userNotificationId = input.NotificationUsers.Where(x => x.Id != Guid.Empty).Select(x => x.Id);
                var newUserNotification = input.NotificationUsers.Where(x => x.Id == Guid.Empty).ToList();
                
                if (listUsers.Any())
                {
                    await _notificationUserRepository.DeleteAsync(x => x.NotificationId == input.Id && !userNotificationId.Contains(x.Id));
                }
                
                foreach (var user in newUserNotification)
                {
                    if (user.NotificationId == Guid.Empty)
                        user.NotificationId = input.Id.GetValueOrDefault();
                    
                    await _notificationUserRepository.InsertAsync(ObjectMapper.Map<CreateUpdateNotificationUserDto, NotificationUser>(user));
                }
                
                await CurrentUnitOfWork.SaveChangesAsync();
                return notification.Id;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        public override async Task Delete(Guid id)
        {
            try
            {
                var notification = await _notificationRepository.FirstOrDefaultAsync(x => x.Id == id);
                if (notification == null)
                    throw new Exception("Item not found");

                await _notificationUserRepository.DeleteAsync(x => x.NotificationId == notification.Id);
                await _notificationRepository.DeleteAsync(notification);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
