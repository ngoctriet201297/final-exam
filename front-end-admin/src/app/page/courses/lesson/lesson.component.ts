import { PagedResultDto } from "@abp/ng.core";
import { AfterViewInit, Component, Injector, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { merge } from "rxjs";
import { startWith, switchMap } from "rxjs/operators";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { LessonDto } from "src/app/shared/dto/lesson";
import { LessonService } from "src/app/shared/service/courses/lesson.service";
declare var $: any;

@Component({
    templateUrl: "./lesson.component.html"
})
export class LessonComponent extends AppBaseComponent implements OnInit, AfterViewInit {

    columnsToDisplay = ["name", "description", "action", 'status']
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource: PagedResultDto<LessonDto>;

    constructor(protected readonly injector: Injector,
        private readonly lessonService: LessonService) {
        super(injector);

    }
    ngAfterViewInit(): void {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page).pipe(
            startWith({}),
            switchMap(() => {
                return this.getDataForTable(this.paginator.pageIndex, this.sort.direction, this.sort.active);
            })
        ).subscribe(result => {
            this.dataSource = result;
        })
    }
    ngOnInit(): void {

    }
    create() {
        this.redirectTo("/admin/courses/lesson/create");
    }
    edit(id: string) {
        this.redirectTo("/admin/courses/lesson/edit/" + id);
    }
    getDataForTable(pageIndex: number = 0, direction, sorting) {
        return this.lessonService.getByPaged({
            maxResultCount: this.paginator.pageSize,
            skipCount: this.paginator.pageIndex * this.paginator.pageSize,
            sorting: sorting ? (sorting + " " + direction) : ""
        });
    }
    openDialogForDelete(lesson: LessonDto){
        if(lesson){
            this.openPopup({
                title: "Cảnh báo",
                message: `Bạn có chắc chắn xoá ${lesson.name}`,
                type: "Warning",
                btnOk: {
                    callBack: () => {
                        this.lessonService.delete(lesson.id).subscribe(() => {
                            this.showSuccess("Bạn xoá dữ liệu thành công");
                            this.dataSource.items = this.dataSource.items.filter(d => d.id !== lesson.id)
                        }, () => this.showError("Bạn xoá dữ liệu thất bại"))
                    },
                }
            })
        }
       
    }

    changeStatus(event, lesson: LessonDto) {
        const status = event.checked ? 1 : 0;
        this.lessonService.toggleStatus(lesson.id).subscribe(() => {
            lesson.status = status;
            this.showSuccess('Thay đổi trạng thái thành công!')
        }, error => {   
            event.checked = !event.checked;
            $('.switch_'+lesson.id).removeClass('mat-checked');
            if(event.checked) {
                $('.switch_'+lesson.id).addClass('mat-checked');
            }
        })
    }
}