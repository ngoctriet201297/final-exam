﻿using System;

namespace Ft.Elearning.Admin.Documents.Dto
{
    public class DocumentDto : BaseEntityDto
    {
        public string Name { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid FileContentId { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
    }
}
