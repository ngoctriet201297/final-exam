﻿using System;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.AssignTests.Dto
{
    public class CreateUpdateAssignTestUserDto : EntityDto<Guid?>
    {
        public Guid AssignTestId { get; set; }
        public int UserId { get; set; }
        public bool IsManager { get; set; }
    }
}