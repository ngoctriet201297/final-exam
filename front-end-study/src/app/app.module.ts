import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxsModule } from '@ngxs/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { APP_ROUTE_PROVIDER } from './route.provider';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientInterceptor } from './shared/http-client-interceptor/http-client-interceptor';
import { SpinnerService } from './shared/services/spinner/spinner.service';
import { FtBackgroundSpinnerComponent } from './shared/components/ft-background-spinner/ft-background-spinner.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxsModule.forRoot(),
    SharedModule
  ],
  declarations: [AppComponent, FtBackgroundSpinnerComponent],
  providers: [{
      provide: HTTP_INTERCEPTORS, useClass: HttpClientInterceptor, multi: true
    },
    SpinnerService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
