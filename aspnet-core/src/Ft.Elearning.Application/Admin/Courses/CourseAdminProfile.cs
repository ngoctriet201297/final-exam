﻿using AutoMapper;
using Ft.Elearning.Admin.Courses.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Courses
{
    public class CourseAdminProfile : Profile
    {
        public CourseAdminProfile()
        {
            CreateMap<CreateUpdateCourseDto, Course>();
            CreateMap<Course, GetCourseForViewDto>();
            CreateMap<Course, GetCourseForEditDto>();
        }
    }
}
