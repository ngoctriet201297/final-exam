﻿using System;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.ComingEvents.Dto
{
    public class CreateUpdateComingEventDto : BaseEntityDto<Guid?>
    {
        [MaxLength(ComingEventConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(ComingEventConsts.MaxLengthContent)]
        public string Content { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
