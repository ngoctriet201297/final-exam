﻿using System;
using Ft.Elearning.Admin.Documents.Dto;
using Volo.Abp.Application.Services;

namespace Ft.Elearning.Admin.Documents
{
    public interface IDocumentAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateDocumentDto, GetDocumentForViewDto, GetDocumentForEditDto, GetDocumentInputDto>
    {

    }
}
