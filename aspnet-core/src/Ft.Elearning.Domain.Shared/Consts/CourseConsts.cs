﻿namespace Ft.Elearning.Consts
{
    public class CourseConsts
    {
        public const int MaxLengthName = 255;
        public const int MaxLengthDescription = 1024;
    }
}