﻿using System;
using Ft.Elearning.Admin.Courses.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Courses
{
    public class CourseAdminAppService : AppBaseAdminService<Course, Guid, CreateUpdateCourseDto, GetCourseForViewDto, GetCourseForEditDto, GetCourseInputDto>,
                                                  ICourseAdminAppService
    {
        public CourseAdminAppService(IRepository<Course, Guid> repository) : base(repository)
        {
        }
    }
}
