using Ft.Elearning.MongoDB;
using Xunit;

namespace Ft.Elearning
{
    [CollectionDefinition(ElearningTestConsts.CollectionDefinitionName)]
    public class ElearningApplicationCollection : ElearningMongoDbCollectionFixtureBase
    {

    }
}
