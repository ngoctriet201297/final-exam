import { Injectable } from '@angular/core';
import { CreateUpdateProductDocumentDto,
    GetProductDocumentForEditDto,
    GetProductDocumentForViewDto,
    GetProductDocumentInputDto } from '../../dto/product-document';
import { AppBaseService } from '../app-base-service';
import { HttpClient } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ProductDocumentService
    extends AppBaseService<
    CreateUpdateProductDocumentDto,
    GetProductDocumentForEditDto,
    GetProductDocumentInputDto,
    GetProductDocumentForViewDto> {

    constructor(protected readonly httpClient: HttpClient){
        super(httpClient, 'document-admin');
    }
}
