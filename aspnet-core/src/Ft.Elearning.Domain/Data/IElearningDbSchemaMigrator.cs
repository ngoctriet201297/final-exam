﻿using System.Threading.Tasks;

namespace Ft.Elearning.Data
{
    public interface IElearningDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
