﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Ft.Elearning.Shared.FileEntities
{
    public interface IFileAppService : IApplicationService
    {
        Task<Guid> Insert();
        Task Delete(Guid id);
        Task<object> Get(Guid id);
    }
}
