import { Component, Injector, Input } from "@angular/core";
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from "@angular/forms";
import { AppBaseComponent } from "../../app-base-component";
import { AppFileService } from "../../service/file/app-file.service";

@Component({
    templateUrl: "./ft-upload-image.component.html",
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: FtUploadImageComponent,
            multi: true
        }
    ],
    selector: "ft-upload-image"
})
export class FtUploadImageComponent extends AppBaseComponent implements ControlValueAccessor {
    @Input() label: string;
    @Input() maxWidth: number;
    uploadForm: FormGroup;
    isProcessing: boolean;
    onChange: (input: string) => any;

    constructor(protected readonly injector: Injector,
        private readonly fb: FormBuilder,
        private readonly uploadFileService: AppFileService) {
        super(injector);
        this.uploadForm = this.fb.group({
            imageId: []
        });
        this.uploadForm.valueChanges.subscribe(newValue => {
            this.onChange && this.onChange(newValue.imageId);
        })
    }

    writeValue(obj: string): void {
        this.uploadForm.patchValue({
            imageId: obj
        })
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
       
    }
    setDisabledState?(isDisabled: boolean): void {
        
    }

    fileChange(event: any) {
        let files = event.target.files;
        if (files.length) {
            var formData = new FormData();
            formData.append("file", files[0]);
            this.isProcessing = true;
            this.uploadFileService.insert(formData).subscribe((result) => {
                this.uploadForm.patchValue({
                    imageId: result
                });
                this.isProcessing = false;
            })
        }
    }
}