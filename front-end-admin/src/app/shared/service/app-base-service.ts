import { PagedResultDto } from "@abp/ng.core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AppHelper } from "../helper/app-helper";

export class AppBaseService<TCreateUpdateDto, TGetForEditDto, TGetInputDto, TGetForViewDto>{

    protected API: string;
    constructor(protected readonly httpClient: HttpClient, protected readonly url: string){
        this.API = environment.apis.default.url + "/api/app/" + url + "/";
    }

    save(input: TCreateUpdateDto): Observable<void>{
        return this.httpClient.post<void>(this.API + "save",input);
    }

    get(id: string): Observable<TGetForEditDto>{
        return this.httpClient.get<TGetForEditDto>(this.API + id);
    }

    delete(id: string): Observable<void>{
        return this.httpClient.delete<void>(this.API + id);
    }

    getByPaged(input: TGetInputDto): Observable<PagedResultDto<TGetForViewDto>>{
        var params = AppHelper.convertObjectToParam(input);
        return this.httpClient.get(this.API + "paged",{
            params: params
        })
    }
}