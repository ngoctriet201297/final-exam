import { Injectable } from "@angular/core";
import { CourseDto, GetCourseInputDto } from '../../dto/course';
import { AppBaseService } from "../app-base-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
@Injectable({
    providedIn: "root"
})
export class CourseService extends AppBaseService<CourseDto, CourseDto, GetCourseInputDto, CourseDto> {
    prefix = environment.apis.default.url + "/api/app"
    constructor(protected readonly httpClient: HttpClient){
        super(httpClient,"course-admin");
    }

    getAll(): Observable<CourseDto[]>{
        return this.httpClient.get<CourseDto[]>(this.prefix + '/course-admin/');
    }

    toggleStatus(id: string) {
        return this.httpClient.post<null>( this.prefix + '/course-admin/' + id + '/toggle-status', null);
    }
}