import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../../dto/api-user/api-response';
import { ApiUserDistributor } from '../../dto/api-user/api-user-distributor';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private readonly httpClient: HttpClient) {
    }

    getDistributor(): Observable<ApiResponse<ApiUserDistributor[]>> {
        return this.httpClient.get('/api/app/user/distributor');
    }

    getUserByDistributor(distributorId: number): Observable<ApiResponse<ApiUserDistributor[]>> {
        return this.httpClient.get('/api/app/user/user-by-distributor' + distributorId);
    }
}
