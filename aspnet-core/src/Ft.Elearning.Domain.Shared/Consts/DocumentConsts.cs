﻿namespace Ft.Elearning.Consts
{
    public class DocumentConsts
    {
        public const int MaxLengthName = 255;
        public const int MaxLengthDescription = 1024;
        public const int MaxLengthContent = 5000;
    }
}