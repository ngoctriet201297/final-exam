﻿using System;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Courses.Dto
{
    public class CreateUpdateCourseDto : BaseEntityDto<Guid?>
    {
        [MaxLength(CourseConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(CourseConsts.MaxLengthDescription)]
        public string Description { get; set; }
        public Guid ThumbnailId { get; set; }
    }
}
