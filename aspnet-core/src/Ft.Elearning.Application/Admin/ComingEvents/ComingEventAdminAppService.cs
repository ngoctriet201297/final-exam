﻿using System;
using Ft.Elearning.Admin.ComingEvents.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.ComingEvents
{
    public class ComingEventAdminAppService : 
        AppBaseAdminService<ComingEvent, Guid, CreateUpdateComingEventDto, GetComingEventForViewDto, GetComingEventForEditDto, GetComingEventInputDto>,
            IComingEventAdminAppService
    {
        public ComingEventAdminAppService(IRepository<ComingEvent, Guid> repository) : base(repository)
        {
        }
    }
}
