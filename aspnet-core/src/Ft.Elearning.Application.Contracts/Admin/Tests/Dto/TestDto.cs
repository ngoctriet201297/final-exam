﻿using System;

namespace Ft.Elearning.Admin.Tests.Dto
{
    public class TestDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PercentFinished { get; set; }
        public int TimeTotal { get; set; }
        public DateTime? EffectiveDateFrom { get; set; }
        public DateTime? EffectiveDateTo { get; set; }
        public Guid ThumbnailId { get; set; }
    }
}
