import { Component, Injector, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { ApiUserDistributor } from "src/app/shared/dto/api-user/api-user-distributor";
import { ProductDocumentService } from "src/app/shared/service/product-document/product-document.service";
import { UserService } from "src/app/shared/service/user/user-service";

@Component({
    templateUrl: "create-edit-product-document.component.html"
})
export class CreateEditProductDocumentComponent extends AppBaseComponent implements OnInit{
    documentForm: FormGroup;
    distributors: ApiUserDistributor[];
    get documentId(): string{
        return this.getParam("id");
    }

    constructor(protected readonly injector: Injector,
                private readonly fb: FormBuilder,
                private readonly productDocumentService: ProductDocumentService,
                private readonly userService: UserService){
        super(injector)

        this.documentForm = fb.group({
            id: [""],
            name: ["",[Validators.required, Validators.maxLength(255)]],
            thumbnailId: [""],
            fileContentId: [""],
            description: [""],
            content: [""]
        });
    }

    get name(){
        return this.documentForm.get("name");
    }

    ngOnInit(): void {
        this.getInitData();
        if(this.documentId){
            this.productDocumentService.get(this.documentId).subscribe((result) => {
                this.documentForm.patchValue({
                    ...result
                })
            })
        }
    }

    save(){
        if(this.documentForm.valid){
            this.setBusy();
            let value = this.documentForm.value;
            if(!value.id) delete value.id;
            this.productDocumentService.save(value).subscribe(() => {
                this.showSuccess("Bạn đã cập nhật thành công");
                this.redirectTo("/admin/product-document");
                this.clearBusy();
            },() => {
                this.showError("Cập nhật thất bại");
                this.clearBusy();
            })
        }
       
    }

    getInitData(){
        // this.userService.getDistributor().subscribe(result => {
        //     this.distributors = result.data;
        // });
    }

    goBack(){
        this.redirectTo("/admin/")
    }
}