import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { CreateEditProductDocumentComponent } from './create-edit/create-edit-product-document.component';
import { ProductDocumentComponent } from './product-document.component';

const routes: Routes = [
    {
        path: "",
        component: ProductDocumentComponent
    },
    {
        path: "create",
        component: CreateEditProductDocumentComponent
    },
    {
        path: "edit/:id",
        component: CreateEditProductDocumentComponent
    }
]
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProductDocumentRouting{}