﻿using System;
using Ft.Elearning.Admin.AssignTests.Dto;

namespace Ft.Elearning.Admin.AssignTests
{
    public interface IAssignTestAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateAssignTestDto, GetAssignTestForViewDto, GetAssignTestForEditDto, GetAssignTestInputDto>
    {

    }
}
