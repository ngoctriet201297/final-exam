﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ft.Elearning.Entities
{
    public class AssignTest : BaseEntity
    {
        public Guid TestId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        
        [ForeignKey("TestId")]
        public Test Test { get; set; }
        
        public ICollection<AssignTestUser> AssignTestUser { get; set; }
    }
}