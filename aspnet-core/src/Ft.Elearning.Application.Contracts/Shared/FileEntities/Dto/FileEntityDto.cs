﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ft.Elearning.Shared.FileEntities.Dto
{
    public class FileEntityDto
    {
        public string FileName { get; set; }
    }
}
