﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Question : BaseEntity
    {
        [MaxLength(QuestionConsts.MaxLengthContent)]
        public string Content { get; set; }
        public Guid? LessonId { get; set; }
        
        [ForeignKey("LessonId")]
        public Lesson Lesson { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}