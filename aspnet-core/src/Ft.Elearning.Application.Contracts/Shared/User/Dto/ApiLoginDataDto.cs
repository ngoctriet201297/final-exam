﻿using System;
using System.Collections.Generic;

namespace Ft.Elearning.Shared.User.Dto
{
    [Serializable]
    public class ApiLoginDataDto
    {
        public string token { get; set; }
        public ApiProfileDto profile { get; set; }
        public string ca_company_user_name { get; set; }
        public string ca_company_logo { get; set; }
        public string ca_user_is_first_password_change { get; set; }
        public List<string> ca_app_permission { get; set; }
    }
}
