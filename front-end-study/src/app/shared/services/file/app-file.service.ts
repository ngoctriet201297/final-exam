import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

const API = environment.apis.default.url + "/api/app/file/";
@Injectable({
    providedIn: "root"
})
export class AppFileService{
    constructor(private readonly httpClient: HttpClient){

    }

    delete(id: string){
        return this.httpClient.delete(API + id);
    }

    get(id: string){
        return this.httpClient.get(API + id);
    }
    insert(file: FormData){
        return this.httpClient.post<string>(API, file);
    }

}