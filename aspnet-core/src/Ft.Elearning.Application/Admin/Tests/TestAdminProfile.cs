﻿using AutoMapper;
using Ft.Elearning.Admin.Tests.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Tests
{
    public class TestAdminProfile : Profile
    {
        public TestAdminProfile()
        {
            CreateMap<CreateUpdateTestDto, Test>();
            CreateMap<CreateUpdateTestQuestionDto, TestQuestion>();
            CreateMap<Test, GetTestForViewDto>();
            CreateMap<Test, GetTestForEditDto>();
        }
    }
}
