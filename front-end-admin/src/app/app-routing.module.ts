import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './page/login/login.component';
import { AuthGuard } from './shared/guard/auth-guard';
import { FtApplicationLayoutComponent } from './shared/layout/ft-application-layout/ft-application-layout.component';

const routes: Routes = [
  {
    path: 'admin',
    component: FtApplicationLayoutComponent,
    children: [
      {
        path: "product-document",
        loadChildren: () => import("./page/product-document/product-document.module").then(x => x.ProductDocumentModule)
      },
      {
        path: "courses",
        loadChildren: () => import("./page/courses/courses.module").then(x => x.CoursesModule)
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    pathMatch: 'full',
    path: '',
    redirectTo: '/admin'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
