import { NgModule } from "@angular/core";
import { SharedModule } from "src/app/shared/shared.module";
import { CreateEditProductDocumentComponent } from "./create-edit/create-edit-product-document.component";
import { ProductDocumentComponent } from "./product-document.component";
import { ProductDocumentRouting } from "./product-document.routing";

@NgModule({
    declarations: [
        ProductDocumentComponent,
        CreateEditProductDocumentComponent
    ],
    imports: [
        ProductDocumentRouting,
        SharedModule
    ],
    exports: [ProductDocumentRouting]
})
export class ProductDocumentModule{}