import { Component, Injector, ViewChild } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material/sidenav';
import { AppBaseComponent } from '../../app-base-component';
@Component({
  templateUrl: './ft-application-layout.component.html'
})
export class FtApplicationLayoutComponent extends AppBaseComponent {

  opened = true;
  over = 'side';
  expandHeight = '42px';
  collapseHeight = '42px';
  displayMode = 'flat';
  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(protected readonly injector: Injector,
              media: MediaObserver) {
    super(injector);
    media.asObservable().subscribe((changes) => {
      if (changes.find(change => change.mqAlias === 'sm' || change.mqAlias === 'xs')) {
        this.opened = false;
        this.over = 'over';
      } else {
        this.opened = true;
        this.over = 'side';
      }
    });
  }

  toggleMenu(){
    this.sidenav.toggle();
  }
}
