import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoursesRoutingModule } from './courses-routing.module';
import { CourseComponent } from './course/course.component';
import { CreateEditCourseComponent } from './course/create-edit/create-edit-course.component';
import { LessonComponent } from './lesson/lesson.component';
import { CreateEditLessonComponent } from './lesson/create-edit/create-edit-lesson.component';

@NgModule({
  declarations: [
    CourseComponent,
    CreateEditCourseComponent,
    LessonComponent,
    CreateEditLessonComponent,
  ],
  imports: [CoursesRoutingModule, SharedModule],
  exports: [CoursesRoutingModule],
})
export class CoursesModule {}
