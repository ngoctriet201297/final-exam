﻿using System;
using Ft.Elearning.Admin.ComingEvents.Dto;

namespace Ft.Elearning.Admin.ComingEvents
{
    public interface IComingEventAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateComingEventDto, GetComingEventForViewDto, GetComingEventForEditDto, GetComingEventInputDto>
    {

    }
}
