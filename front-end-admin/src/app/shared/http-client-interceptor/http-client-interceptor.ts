import { isNumber } from '@abp/ng.core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AccountService } from '../service/account/account.service';
import { SpinnerService } from '../service/spinner/spinner.service';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {

    private get accessToken(): string {
        const expiredAt = localStorage.getItem('token_expire');
        if (!expiredAt) { return null; }
        if (!isNumber(expiredAt)) { return null; }
        if (Number(expiredAt) < new Date().getTime()) { return null; }
        return localStorage.getItem('token');
    }
    constructor(private readonly accountService: AccountService,
                private readonly toastService: ToastrService,
                private readonly spinerService: SpinnerService) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(this.addHeader(req))
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    if (error.status === 401) {
                        this.accountService.logout();
                        window.location.href = window.location.origin;
                        return throwError(`${error.status} ${error.message}`);
                    }
                    let errorMessage = '';
                    if (error.error?.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${error.error?.error?.message}`;
                    } else {
                        // server-side error
                        errorMessage = `${error.error?.error?.message}`;
                    }
                    this.toastService.error(errorMessage);
                    this.spinerService.isProcessing = false;
                    return throwError(errorMessage);
                }),
                map(resp => {
                   // this.spinerService.isProcessing = false;
                    return resp;
                })
            );
    }

    private addHeader(request: HttpRequest<any>) {

        request = request.clone({
            headers: request.headers.set('locale', `vi`)
        });

        if (!request.url.startsWith('http')) {
            const url = environment.apis.default.url;
            request = request.clone({
                url: url + request.url
            });
        }
        
        if (this.accessToken) {
            request = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${this.accessToken}`)
            });
        }

        return request;
    }
}
