import { HttpParams } from "@angular/common/http";

export class AppHelper {
    static convertObjectToParam(input: any): HttpParams {
        let params = new HttpParams();
        if (!input) return params;
        Object.keys(input).forEach(key => {
            params = params.append(key, input[key]);
        });
        return params;
    }
    static generateGuid(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {  
            var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);  
            return v.toString(16);  
         });  
    }
}