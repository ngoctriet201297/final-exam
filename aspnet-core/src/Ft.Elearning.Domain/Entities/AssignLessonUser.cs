﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Ft.Elearning.Entities
{
    public class AssignLessonUser : Entity<Guid?>
    {
        public Guid AssignLessonId { get; set; }
        public int UserId { get; set; }
        public bool IsManager { get; set; }
        
        [ForeignKey("AssignLessonId")]
        public AssignLesson AssignLesson { get; set; }
    }
}