﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ft.Elearning.Admin.Questions.Dto;

namespace Ft.Elearning.Admin.Questions
{
    public interface IQuestionAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateQuestionDto, GetQuestionForViewDto, GetQuestionForEditDto, GetQuestionInputDto>
    {
    }
}
