﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Answer : BaseEntity<Guid?>
    {
        [MaxLength(AnswerConsts.MaxLengthContent)]
        public string Content { get; set; }
        public bool IsRightAnswer { get; set; }
        public Guid QuestionId { get; set; }
        
        [ForeignKey("QuestionId")]
        public Question Question { get; set; }
    }
}