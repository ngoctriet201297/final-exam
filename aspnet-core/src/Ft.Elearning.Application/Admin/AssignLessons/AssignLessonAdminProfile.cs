﻿using AutoMapper;
using Ft.Elearning.Admin.AssignLessons.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.AssignLessons
{
    public class AssignLessonAdminProfile : Profile
    {
        public AssignLessonAdminProfile()
        {
            CreateMap<CreateUpdateAssignLessonDto, AssignLesson>();
            CreateMap<CreateUpdateAssignLessonUserDto, AssignLessonUser>();
            CreateMap<AssignLesson, GetAssignLessonForViewDto>();
            CreateMap<AssignLesson, GetAssignLessonForEditDto>();
        }
    }
}
