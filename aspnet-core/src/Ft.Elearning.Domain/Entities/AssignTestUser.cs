﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Ft.Elearning.Entities
{
    public class AssignTestUser : Entity<Guid?>
    {
        public Guid AssignTestId { get; set; }
        public int UserId { get; set; }
        public bool IsManager { get; set; }
        
        [ForeignKey("AssignTestId")]
        public AssignTest AssignTest { get; set; }
    }
}