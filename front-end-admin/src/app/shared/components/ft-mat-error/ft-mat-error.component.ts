import { animate, style, transition, trigger } from "@angular/animations";
import { Component, Input } from "@angular/core";

@Component({
    templateUrl: "./ft-mat-error.component.html",
    animations: [
        trigger('animation', [
            transition(':increment',[
                style({opacity:0}),
                animate('200ms ease-in', style({opacity: 1}))
            ]),
            transition(':enter', [
                style({opacity: 0, transform: 'translateY(-1rem)'}),
                animate('200ms  ease-in', style({opacity: 0, transform: 'translateY(0)'}))
            ]),
            transition(':leave', [
                animate('200ms ease-out', style({opacity: 0, transform: 'translateY(-1rem)'}))
            ])
        ])
    ]
})
export class FtMatErrorComponent{
    message: string;
    increment = 0;

    @Input()
    set error(value){
        if(value){
            if(this.message !== value) this.increment++;
        }
        this.message = value;
    }
}