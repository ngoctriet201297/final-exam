﻿using System;
using Volo.Abp.Data;
using Volo.Abp.Modularity;

namespace Ft.Elearning.MongoDB
{
    [DependsOn(
        typeof(ElearningTestBaseModule),
        typeof(ElearningMongoDbModule)
        )]
    public class ElearningMongoDbTestModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var stringArray = ElearningMongoDbFixture.ConnectionString.Split('?');
                        var connectionString = stringArray[0].EnsureEndsWith('/')  +
                                                   "Db_" +
                                               Guid.NewGuid().ToString("N") + "/?" + stringArray[1];

            Configure<AbpDbConnectionOptions>(options =>
            {
                options.ConnectionStrings.Default = connectionString;
            });
        }
    }
}
