import { Component, EventEmitter, Injector, Input, Output } from "@angular/core";
import { AppBaseComponent } from '../../app-base-component';

@Component({
    templateUrl: "ft-layout-header.component.html",
    selector: "ft-layout-header"
})
export class FtLayoutHeaderComponent extends AppBaseComponent {
    constructor(protected readonly injector: Injector) {
        super(injector);
    }

    @Output()
    menuClick: EventEmitter<any> = new EventEmitter();
    @Input()
    showBtn: boolean;

    menuBtnClick() {
        this.menuClick.emit();
    }

    logOut() {
        this.openPopup({
            message: "Bạn có chắc muốn đăng xuất?",
            title: "Thông tin",
            type: 'Question',
            btnClose: {},
            btnOk: {
                callBack: () => {
                    this.clearLocalStorage();
                    this.redirectTo("/login");
                }
            }
        })
    }
}