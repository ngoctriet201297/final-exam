﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Notification : BaseEntity
    {
        [MaxLength(NotificationConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(NotificationConsts.MaxLengthContent)]
        public string Content { get; set; }
        public ICollection<NotificationUser> NotificationUsers { get; set; }
    }
}