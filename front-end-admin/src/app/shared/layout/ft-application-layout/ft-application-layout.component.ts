import { Component, HostListener, Injector, OnInit, ViewChild } from "@angular/core";
import { Observable, Subscription } from 'rxjs';
import { MediaChange, MediaObserver } from "@angular/flex-layout";
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { FtSidenavComponent } from '../ft-sidenav/ft-sidenav.component';
import { MatSidenav } from '@angular/material/sidenav';
import { AppBaseComponent } from '../../app-base-component';
import * as moment from "moment";

@Component({
  templateUrl: "./ft-application-layout.component.html"
})
export class FtApplicationLayoutComponent extends AppBaseComponent implements OnInit{

  opened = true;
  over = 'side';
  expandHeight = '42px';
  collapseHeight = '42px';
  displayMode = 'flat';
  @ViewChild("sidenav") sidenav: MatSidenav;

  constructor(protected readonly injector: Injector,
    private readonly media: MediaObserver) {
    super(injector);
    media.asObservable().subscribe((changes) => {
      if (changes.find(change => change.mqAlias === 'sm' || change.mqAlias === 'xs')) {
        this.opened = false;
        this.over = 'over';
      } else {
        this.opened = true;
        this.over = 'side';
      }
    });
  }
  ngOnInit(): void {
    setInterval(() => {
      console.log(moment().isBefore(moment(this.tokenInfo?.token_expire)));
      console.log(moment().isAfter(moment(this.tokenInfo?.token_expire)));
      if( moment().isAfter(moment(this.tokenInfo?.token_expire))){
        this.clearLocalStorage();
        this.redirectTo("/login");
      }
    }, 1000)
  }


  toggleMenu(){
    this.sidenav.toggle();
  }

}