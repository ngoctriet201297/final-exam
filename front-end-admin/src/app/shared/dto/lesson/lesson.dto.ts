import { BaseEntityDto } from '../base-entity.dto';

export class LessonDto extends BaseEntityDto<string>{
    public id: string;
    public thumbnailId: string;
    public name: string;
    public description: string;
    public content: string;
    public courseId: string;
}