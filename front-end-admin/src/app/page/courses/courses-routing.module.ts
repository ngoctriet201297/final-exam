import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourseComponent } from './course/course.component';
import { CreateEditCourseComponent } from './course/create-edit/create-edit-course.component';
import { LessonComponent } from './lesson/lesson.component';
import { CreateEditLessonComponent } from './lesson/create-edit/create-edit-lesson.component';
const routes: Routes = [
  {
    path: 'course',
    children: [
      {
        path: '',
        component: CourseComponent,
      },
      {
        path: 'create',
        component: CreateEditCourseComponent,
      },
      {
        path: 'edit/:id',
        component: CreateEditCourseComponent,
      },
    ],
  },
  {
    path: 'lesson',
    children: [
      {
        path: '',
        component: LessonComponent,
      },
      {
        path: 'create',
        component: CreateEditLessonComponent,
      },
      {
        path: 'edit/:id',
        component: CreateEditLessonComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursesRoutingModule {}
