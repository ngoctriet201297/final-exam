﻿using System;
using System.Threading.Tasks;
using Ft.Elearning.Helper;
using Ft.Elearning.Models;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.ResponseHandling;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Guids;
using Volo.Abp.Identity;

namespace Ft.Elearning.Controllers
{
    [Area("Login")]
    [Route("User")]
    public class UserController : AbpController
    {
        private readonly IdentityUserManager _userManager;
        private readonly IdentityRoleManager _roleManager;
        private readonly IGuidGenerator _guidGenerator;
        private readonly IdentityServerTools _identityServerTools;

        public UserController(IdentityUserManager userManager, IdentityRoleManager roleManager,
                              IGuidGenerator guidGenerator,
                              IdentityServerTools identityServerTools)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _guidGenerator = guidGenerator;
            _identityServerTools = identityServerTools;
        }

        [HttpPost("loginWithFB")]
        public async Task<LoginFBOutputDto> LoginWithFB([FromBody] LoginWithFBInputDto input)
        {
            var user = await _userManager.FindByEmailAsync(input.Email);
            if (user != null)
            {

            }
            else
            {
                user = new IdentityUser(_guidGenerator.Create(), input.Id, input.Email)
                {
                    Name = input.Name
                };
                var password = GenerateHelper.GeneratePassword();
                var createdUser = await _userManager.CreateAsync(user, password);
                if (!createdUser.Succeeded) throw new UserFriendlyException("");
            }

            var token = await _identityServerTools.IssueClientJwtAsync(input.ClientId, 180);
            return new LoginFBOutputDto
            {
                access_token = token,
                expires_in = 3
            };
        }
    }
}
