﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Lesson : BaseEntity
    {
        [MaxLength(LessonConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(LessonConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(LessonConsts.MaxLengthContent)]
        public string Content { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid CourseId { get; set; }
        
        [ForeignKey("CourseId")]
        public Course Course { get; set; }
        
        [ForeignKey("ThumbnailId")]
        public FileEntity Thumbnail { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}