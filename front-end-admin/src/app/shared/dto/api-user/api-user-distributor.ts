export class ApiUserDistributor {
    user_id?: number;
    user_name?: string;
    user_full_name?: string;
    user_phone?: string;
    user_distributor_id?: number;
    distributor_id?: number;
    distributor_name?: string;
}
