﻿using System;
using Ft.Elearning.Admin.Tests.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Tests
{
    public class TestAdminAppService : AppBaseAdminService<Test, Guid, CreateUpdateTestDto, GetTestForViewDto, GetTestForEditDto, GetTestInputDto>,
                                                  ITestAdminAppService
    {
        public TestAdminAppService(IRepository<Test, Guid> repository) : base(repository)
        {
        }
    }
}
