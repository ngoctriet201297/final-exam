import { EntityDto } from "@abp/ng.core";
import { Status } from './status';

export class BaseEntityDto<T> extends EntityDto<T>{
    public status: Status;
    public distributorId: number;
}
