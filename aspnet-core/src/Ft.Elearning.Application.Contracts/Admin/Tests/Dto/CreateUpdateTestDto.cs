﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Tests.Dto
{
    public class CreateUpdateTestDto : BaseEntityDto<Guid?>
    {
        [Required]
        [MaxLength(TestConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(TestConsts.MaxLengthTitle)]
        public string Title { get; set; }
        [MaxLength(TestConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [Required]
        public int PercentFinished { get; set; }
        [Required]
        public int TimeTotal { get; set; }
        [Required]
        public DateTime? EffectiveDateFrom { get; set; }
        [Required]
        public DateTime? EffectiveDateTo { get; set; }
        public Guid ThumbnailId { get; set; }
        
        public List<CreateUpdateTestQuestionDto> TestQuestions { get; set; }
    }
}
