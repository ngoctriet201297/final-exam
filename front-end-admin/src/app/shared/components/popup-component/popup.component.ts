import { AfterViewInit, Component, ComponentFactoryResolver, Inject, Injector, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AnchorDirective } from '../../directive/anchor-directive/anchor.directive';
import { ErrorMessagePopupComponent } from './message-component/error-message-popup.component';
import { InformationMessagePopupComponent } from './message-component/information-message-popup.component';
import { QuestionMessagePopupComponent } from './message-component/question-message-popup.component';
import { WarningMessagePopupComponent } from './message-component/warning-message-popup.component';

@Component({
    templateUrl: './popup.component.html'
})
export class PopupComponent implements AfterViewInit{

    @ViewChild(AnchorDirective) anchorDirective: AnchorDirective;

    constructor(@Inject(MAT_DIALOG_DATA) public data: IOptionPopup,
                private readonly componentFactory: ComponentFactoryResolver){
    }
    ngAfterViewInit(): void {
        this.loadMessage();
    }

    loadMessage(){
        let component;
        switch (this.data?.type){
            case 'Error':
                component = ErrorMessagePopupComponent;
                break;
            case 'Warning':
                component = WarningMessagePopupComponent;
                break;
            case 'Question':
                component = QuestionMessagePopupComponent;
                break;
            default:
                component = InformationMessagePopupComponent;
        }
        const componentFactory = this.componentFactory.resolveComponentFactory(component);

        const viewContainerRef = this.anchorDirective.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent(componentFactory) as any;
        componentRef.instance.message = this.data?.message;
        componentRef.instance.title = this.data?.title;
    }
}

export interface IOptionPopup{
    title: string;
    message: string;
    btnClose?: {
        label?: string,
        callBack?: () => any
    };
    btnOk?: {
        label?: string,
        callBack?: () => any
    };
    type?: 'Information' | 'Warning' | 'Error' | 'Question';
}
