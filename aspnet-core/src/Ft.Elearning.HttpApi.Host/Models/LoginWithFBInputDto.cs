﻿using System;
namespace Ft.Elearning.Models
{
    public class LoginWithFBInputDto
    {
        public string ClientId { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public string Provider { get; set; }
        public string Name { get; set; }
    }
}
