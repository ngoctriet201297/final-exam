export class UserLoginOutputDto{
    access_token: string;
    fullName: string;
    expires_in: number;
}
