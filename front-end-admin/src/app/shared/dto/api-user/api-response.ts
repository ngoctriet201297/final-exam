export class ApiResponse<T> {
    code?: number;
    data_summary?: number;
    data?: T;
    message?: string;
}
