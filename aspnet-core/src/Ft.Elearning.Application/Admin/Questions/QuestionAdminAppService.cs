﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ft.Elearning.Admin.Answers.Dto;
using Ft.Elearning.Admin.Questions.Dto;
using Ft.Elearning.Entities;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Questions
{
    public class QuestionAdminAppService : AppBaseAdminService<Question, Guid, CreateUpdateQuestionDto,
            GetQuestionForViewDto, GetQuestionForEditDto, GetQuestionInputDto>,
        IQuestionAdminAppService
    {
        private readonly IRepository<Answer, Guid?> _answerRepository;
        public QuestionAdminAppService(IRepository<Question, Guid> repository,
            IRepository<Answer, Guid?> answerRepository) : base(repository)
        {
            _answerRepository = answerRepository;
        }

        protected override IQueryable<Question> CreateFilteredQuery(GetQuestionInputDto input)
        {
            return Repository.WhereIf(input.LessonId.HasValue, x => x.LessonId == input.LessonId);
        }

        protected override async Task<Guid> Update(CreateUpdateQuestionDto input)
        {
            var question = await Repository.FindAsync(input.Id.Value);

            if (question == null) throw new UserFriendlyException("NotFound");

            question.Content = input.Content;
            question.LessonId = input.LessonId;

            await CurrentUnitOfWork.SaveChangesAsync();

            await _answerRepository.DeleteAsync(x => x.QuestionId == input.Id);

            var listUpdate = input.Answers.Where(x => x.Id.HasValue).ToList();
            var listInsert = input.Answers.Where(x => !x.Id.HasValue).Select(ObjectMapper.Map<CreateUpdateAnswerDto, Answer>).ToList();

            if (listInsert.Any())
            {
                foreach (var answer in listInsert)
                {
                    answer.QuestionId = question.Id;
                    await _answerRepository.InsertAsync(answer);
                }
            }

            if (listUpdate.Any())
            {
                foreach (var item in listUpdate)
                {
                    var answer = await _answerRepository.FindAsync(x => x.Id == item.Id);
                    if (answer == null) continue;

                    answer.Content = item.Content;
                    answer.IsRightAnswer = item.IsRightAnswer;

                    await CurrentUnitOfWork.SaveChangesAsync();
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            return question.Id;
        }

        public override async Task<GetQuestionForEditDto> Get(Guid id)
        {
            var result = await Repository
                .FirstOrDefaultAsync(x => x.Id == id);

            return MapToEditOutput(result);
        }
    }
}
