﻿using AutoMapper;
using Ft.Elearning.Admin.Answers.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Answers
{
    public class AnswerAdminProfile : Profile
    {
        public AnswerAdminProfile()
        {
            CreateMap<CreateUpdateAnswerDto, Answer>();
            CreateMap<Answer, GetAnswerForViewDto>();
            CreateMap<Answer, GetAnswerForEditDto>();
        }
    }
}
