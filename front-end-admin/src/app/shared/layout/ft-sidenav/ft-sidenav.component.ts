import { Component, Injector, ViewChild } from "@angular/core";
import { AppBaseComponent } from '../../app-base-component';

@Component({
  templateUrl: "./ft-sidenav.component.html",
  selector: "ft-sidenav",
  styleUrls: ["./ft-sidenav.component.css"]
})
export class FtSidenavComponent extends AppBaseComponent {
  constructor(protected readonly injector: Injector) {
    super(injector);
  }

  expandHeight = '42px';
  collapseHeight = '42px';
  displayMode = 'flat';

  
}