﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ft.Elearning.Shared.User.Dto;
using Volo.Abp.Application.Services;

namespace Ft.Elearning.Shared.User
{
    public interface IUserService: IApplicationService
    {
        Task<ApiResponse<ApiLoginDataDto>> Login(ApiLoginRequest request);
    }
}
