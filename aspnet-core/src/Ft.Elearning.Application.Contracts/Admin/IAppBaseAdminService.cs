﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Ft.Elearning.Admin
{
    public interface IAppBaseAdminService
        <TPrimaryKey,
        in TCreateUpdate,
        TViewOutput,
        TEditOutput,
        in TGetAllInput>
        : IApplicationService
        where TPrimaryKey : struct
        where TCreateUpdate : BaseEntityDto<TPrimaryKey?>
        where TViewOutput : BaseEntityDto<TPrimaryKey>
        where TEditOutput : BaseEntityDto<TPrimaryKey?>
        where TGetAllInput : PagedAndSortedResultRequestDto
    {
        Task ToggleStatus(TPrimaryKey id);
        Task<PagedResultDto<TViewOutput>> GetListPaged(TGetAllInput input);
        Task<TEditOutput> Get(TPrimaryKey id);
        Task Delete(TPrimaryKey id);
        Task<TPrimaryKey> Save(TCreateUpdate input);
        Task<List<TViewOutput>> GetAll();
    }
}
