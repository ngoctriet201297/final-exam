﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Ft.Elearning.Entities;
using Ft.Elearning.Enums;
using Ft.Elearning.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Shared.FileEntities
{
    public class FileAppService : ElearningAppService, IFileAppService
    {
        private readonly IRepository<FileEntity, Guid> _fileRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string ResourcePath;
        public FileAppService(
            IRepository<FileEntity, Guid> fileRepository,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor)
        {
            _fileRepository = fileRepository;
            _httpContextAccessor = httpContextAccessor;
            ResourcePath = Path.Combine(Directory.GetCurrentDirectory(), configuration["Resources"]);
        }

        public async Task Delete(Guid id)
        {
            await _fileRepository.DeleteAsync(id);
        }

        public async Task<object> Get(Guid id)
        {
            var fileEntity = await _fileRepository.GetAsync(id);
            if (fileEntity != null)
            {
                var path = GetPath(fileEntity.CreationTime, fileEntity.FileName);

                var fileExtension = Path.GetExtension(path);

                FileStream stream = new FileStream(path, FileMode.Open);
                FileStreamResult result = new FileStreamResult(stream, FileHelper.GetMimeType(fileExtension));
                return result;
            }
            return null;
        }

        public async Task<Guid> Insert()
        {
            var fileEntity = await _fileRepository.InsertAsync(new FileEntity());
            await CurrentUnitOfWork.SaveChangesAsync();

            var file = _httpContextAccessor.HttpContext.Request.Form.Files[0];

            if (file.Length > 0)
            {
                var fileExtension = Path.GetExtension(ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim()).Replace("\"", "");
                var fileName = fileEntity.Id + fileExtension;
                var fullPath = CreateFullPath(fileEntity.CreationTime, fileName);
                fileEntity.FileName = fileName;
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                await _fileRepository.UpdateAsync(fileEntity);
            }
            return fileEntity.Id;
        }

        private string CreateFullPath(DateTime date, string fileName)
        {

            var fullPath = Path.Combine(ResourcePath, $"{date.Year}/{date.Month}/{date.Day}");
            Directory.CreateDirectory(fullPath);
            return fullPath + $"/{fileName}";
        }

        private string GetPath(DateTime date, string fileName)
        {
            date = date.AddHours(7);
            return Path.Combine(ResourcePath, $"{date.Year}/{date.Month}/{date.Day}/{fileName}");
        }
    }
}

