﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ft.Elearning.Admin.AssignLessons.Dto
{
    public class CreateUpdateAssignLessonDto : BaseEntityDto<Guid?>
    {
        [Required]
        public Guid LessonId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        [Required]
        public DateTime? From { get; set; }
        [Required]
        public DateTime? To { get; set; }

        public List<CreateUpdateAssignLessonUserDto> AssignUsers { get; set; }
    }
}
