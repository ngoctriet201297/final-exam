﻿using System;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Shared.FileEntities.Dto
{
    public class CreateUpdateFileEntityDto : EntityDto<Guid>
    {
        public string FileName { get; set; }
    }
}