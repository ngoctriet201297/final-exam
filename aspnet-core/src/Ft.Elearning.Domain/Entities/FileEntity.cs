﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;
using Volo.Abp.Domain.Entities.Auditing;

namespace Ft.Elearning.Entities
{
    public class FileEntity : AuditedAggregateRoot<Guid>
    {
        [MaxLength(FileEntityConsts.FileNameMaxLength)]
        public string FileName { get; set; }
        
        [InverseProperty("Thumbnail")]
        public ICollection<Document> Thumbnails { get; set; } 
        
        [InverseProperty("FileContent")]
        public ICollection<Document> FileContents { get; set; } 
    }
}
