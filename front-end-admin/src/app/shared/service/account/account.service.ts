import { Injectable } from '@angular/core';
import { UserLoginInputDto } from '../../dto/account/user-login-input.dto';
import { UserLoginOutputDto } from '../../dto/account/user-login-output.dto';
import { HttpClient } from '@angular/common/http';
import { AppHelper } from '../../helper/app-helper';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
@Injectable({
    providedIn: 'root'
})
export class AccountService {
    constructor(private readonly httpClient: HttpClient) {
    }

    login(user: UserLoginInputDto, callBack: () => void) {
        let body = {
            client_id: environment.oAuthConfig.clientId,
            client_secret: environment.oAuthConfig.dummyClientSecret,
            username: user.username,
            password: user.password,
            grant_type: environment.oAuthConfig.responseType,
            scope: environment.oAuthConfig.scope
        }

        return this.httpClient.post('/connect/token', AppHelper.convertObjectToStringQuery(body), {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).subscribe((result: UserLoginOutputDto) => {
            if (result) {
                localStorage.setItem('token', result.access_token);
                localStorage.setItem('token_expire', moment().add(3,'minutes').format() );

                if (callBack) {
                    callBack();
                }
            }
        }, (error) => {
            console.log(error);
        });
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('token_expire');
        localStorage.removeItem('user_fullname');
    }

    loginWithFb(input: {id: string, email: string, name: string}, callback: () => any){
        let body = {
            clientId: environment.oAuthConfig.clientId,
            "id": input.id,
            "email": input.email,
            "name": input.name
          }
        return this.httpClient.post('/user/loginWithFB', body).subscribe((result: UserLoginOutputDto) => {
           
            localStorage.setItem('token', result.access_token);
            localStorage.setItem('token_expire', moment().add(result.expires_in,'minutes').format());
            callback && callback();
        })
    }
}