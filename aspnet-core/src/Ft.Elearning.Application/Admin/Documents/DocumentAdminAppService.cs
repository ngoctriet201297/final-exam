﻿using System;
using Ft.Elearning.Admin.Documents.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Documents
{
    public class DocumentAdminAppService : AppBaseAdminService<Document, Guid, CreateUpdateDocumentDto, GetDocumentForViewDto, GetDocumentForEditDto, GetDocumentInputDto>,
                                                  IDocumentAdminAppService
    {
        public DocumentAdminAppService(IRepository<Document, Guid> repository) : base(repository)
        {
        }
    }
}
