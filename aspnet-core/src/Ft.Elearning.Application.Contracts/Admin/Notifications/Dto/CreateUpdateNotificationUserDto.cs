﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.Notifications.Dto
{
    public class CreateUpdateNotificationUserDto: EntityDto<Guid?>
    {
        [Required]
        public Guid NotificationId { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}