﻿using System;

namespace Ft.Elearning.Admin.Courses.Dto
{
    public class CourseDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ThumbnailId { get; set; }
    }
}
