import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocumentRoutingModule } from './document-routing.module';
import { DocumentComponent } from './document.component';

@NgModule({
  declarations: [DocumentComponent],
  imports: [SharedModule, DocumentRoutingModule],
})
export class DocumentModule {}
