import { Component, Input } from "@angular/core";

@Component({
    template: `<div class="d-flex text-info" mat-dialog-title><mat-icon>info</mat-icon> {{title}}</div>
    <div class="text-center message" mat-dialog-content>
    {{message}}
    </div>`
})
export class InformationMessagePopupComponent{
    @Input() message: string;
    @Input() title: string;
}