﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.Tests.Dto
{
    public class CreateUpdateTestQuestionDto: EntityDto<Guid?>
    {
        public Guid TestId { get; set; }
        [Required]
        public Guid QuestionId { get; set; }
    }
}