﻿using System;

namespace Ft.Elearning.Shared.User.Dto
{
    [Serializable]
    public class ApiLoginRequest
    {
        public string ca_user_name { get; set; }
        public string ca_user_password { get; set; }
    }
}
