﻿using System;
using System.Collections.Generic;
using System.Text;
using Ft.Elearning.Localization;
using Volo.Abp.Application.Services;

namespace Ft.Elearning
{
    /* Inherit your application services from this class.
     */
    public abstract class ElearningAppService : ApplicationService
    {
        protected ElearningAppService()
        {
            LocalizationResource = typeof(ElearningResource);
        }
    }
}
