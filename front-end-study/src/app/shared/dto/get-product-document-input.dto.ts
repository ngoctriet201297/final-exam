import { PagedAndSortedResultRequestDto } from "@abp/ng.core";

export class GetProductDocumentInputDto extends PagedAndSortedResultRequestDto{
    public filter: string;
}