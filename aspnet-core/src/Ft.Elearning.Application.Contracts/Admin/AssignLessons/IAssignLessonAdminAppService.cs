﻿using System;
using Ft.Elearning.Admin.AssignLessons.Dto;

namespace Ft.Elearning.Admin.AssignLessons
{
    public interface IAssignLessonAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateAssignLessonDto, GetAssignLessonForViewDto, GetAssignLessonForEditDto, GetAssignLessonInputDto>
    {

    }
}
