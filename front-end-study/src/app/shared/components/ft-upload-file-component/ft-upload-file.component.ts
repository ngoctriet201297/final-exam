import { Component, Injector, OnInit } from "@angular/core";
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR } from "@angular/forms";
import { AppBaseComponent } from "../../app-base-component";
import { AppFileService } from "../../services/file/app-file.service";
@Component({
    templateUrl: "./ft-upload-file.component.html",
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: FtUploadFileComponent,
            multi: true
        }
    ],
    selector: "ft-upload-file"
})
export class FtUploadFileComponent extends AppBaseComponent implements ControlValueAccessor, OnInit {

    fileControl = new FormControl();
    onChange: (input: string) => void;
    isProcessing: boolean;

    constructor(protected readonly injector: Injector,
        private readonly fileService: AppFileService) {
        super(injector);
    }
    ngOnInit(): void {
        this.fileControl.valueChanges.subscribe(newValue => {
            this.onChange && this.onChange(newValue);
        });
    }

    writeValue(obj: string): void {
        this.fileControl.setValue(obj);
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {

    }
    setDisabledState?(isDisabled: boolean): void {

    }
    fileChange(event: any) {
        if (event?.target?.files?.length) {
            let formData = new FormData();
            let file = event.target.files[0];
            formData.append("file", file);
            this.isProcessing = true;
            this.fileService.insert(formData).subscribe(result => {
                this.fileControl.setValue(result);
                this.isProcessing = false;
            })
        }
    }
}