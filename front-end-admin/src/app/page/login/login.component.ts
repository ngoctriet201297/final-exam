import { Component, Injector, OnInit } from '@angular/core';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { UserLoginInputDto } from 'src/app/shared/dto/account/user-login-input.dto';
import { AccountService } from 'src/app/shared/service/account/account.service';
import { environment } from 'src/environments/environment';
@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent extends AppBaseComponent implements OnInit {

    vm: UserLoginInputDto = environment.production ? {} : {
        username: 'admin',
        password: '1q2w3E*'
    };

    constructor(protected readonly injector: Injector,
        private readonly accountService: AccountService,
        private readonly authService: SocialAuthService) {
        super(injector);
    }
    login() {
        this.setBusy();
        this.accountService.login(this.vm, () => {
            this.redirectTo('/');
            this.clearBusy();
        });
    }

    signInWithFB() {
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(user => {
            this.accountService.loginWithFb({
                id: user.id,
                email: user.email,
                name: user.name
            }, () => {
                this.redirectTo("/admin");
            });
        });
    }
    signInWithGoogle() {
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(user => {
            this.accountService.loginWithFb({
                id: user.id,
                email: user.email,
                name: user.name
            }, () => {
                this.redirectTo("/admin");
            });
        });
    }

    signOut(): void {
        this.authService.signOut();
    }

    ngOnInit() {
        this.authService.authState.subscribe((user) => {
           
        });
        if (this.tokenInfo.token) {
            this.redirectTo("/admin")
        }
    }
}
