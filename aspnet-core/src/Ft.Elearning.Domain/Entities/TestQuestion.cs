﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Ft.Elearning.Entities
{
    public class TestQuestion : Entity<Guid?>
    {
        public Guid TestId { get; set; }
        public Guid QuestionId { get; set; }
        
        [ForeignKey("TestId")]
        public Test Test { get; set; }
        
        [ForeignKey("QuestionId")]
        public Question Question { get; set; }
    }
}