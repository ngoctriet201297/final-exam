import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './test.component';
import { ResultComponent } from './result/result.component';

const routes: Routes = [
  { path: '', component: TestComponent },
  { path: 'result', component: ResultComponent  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestRoutingModule {}
