﻿using System;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Documents.Dto
{
    public class CreateUpdateDocumentDto : BaseEntityDto<Guid?>
    {
        [MaxLength(DocumentConsts.MaxLengthName)]
        public string Name { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid FileContentId { get; set; }
        [MaxLength(DocumentConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(DocumentConsts.MaxLengthContent)]
        public string Content { get; set; }
    }
}
