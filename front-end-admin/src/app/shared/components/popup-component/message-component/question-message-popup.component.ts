import { Component, Input } from '@angular/core';

@Component({
    template: `<div class="d-flex text-primary" mat-dialog-title><mat-icon>help</mat-icon> {{title}}</div>
    <div class="text-center message" mat-dialog-content>
    {{message}}
    </div>`
})
export class QuestionMessagePopupComponent {
    @Input() message: string;
    @Input() title: string;
}
