﻿using System;

namespace Ft.Elearning.Admin.Lessons.Dto
{
    public class LessonDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid CourseId { get; set; }
    }
}
