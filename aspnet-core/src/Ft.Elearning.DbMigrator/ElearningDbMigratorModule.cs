using Ft.Elearning.MongoDB;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Ft.Elearning.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(ElearningMongoDbModule),
        typeof(ElearningApplicationContractsModule)
        )]
    public class ElearningDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
