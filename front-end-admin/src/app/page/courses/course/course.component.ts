import { PagedResultDto } from "@abp/ng.core";
import { AfterViewInit, Component, Injector, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { merge } from "rxjs";
import { startWith, switchMap } from "rxjs/operators";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { CourseDto } from "src/app/shared/dto/course";
import { CourseService } from "src/app/shared/service/courses/course.service";
declare var $: any;

@Component({
    templateUrl: "./course.component.html"
})
export class CourseComponent extends AppBaseComponent implements OnInit, AfterViewInit {

    columnsToDisplay = ["name", "description", "action", 'status']
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource: PagedResultDto<CourseDto>;

    constructor(protected readonly injector: Injector,
        private readonly CourseService: CourseService) {
        super(injector);

    }
    ngAfterViewInit(): void {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page).pipe(
            startWith({}),
            switchMap(() => {
                return this.getDataForTable(this.paginator.pageIndex, this.sort.direction, this.sort.active);
            })
        ).subscribe(result => {
            this.dataSource = result;
        })
    }
    ngOnInit(): void {

    }
    create() {
        this.redirectTo("/admin/courses/course/create");
    }
    edit(id: string) {
        this.redirectTo("/admin/courses/course/edit/" + id);
    }
    getDataForTable(pageIndex: number = 0, direction, sorting) {
        return this.CourseService.getByPaged({
            maxResultCount: this.paginator.pageSize,
            skipCount: this.paginator.pageIndex * this.paginator.pageSize,
            sorting: sorting ? (sorting + " " + direction) : ""
        });
    }
    openDialogForDelete(course: CourseDto){
        if(course){
            this.openPopup({
                title: "Cảnh báo",
                message: `Bạn có chắc chắn xoá ${course.name}`,
                type: "Warning",
                btnOk: {
                    callBack: () => {
                        this.CourseService.delete(course.id).subscribe(() => {
                            this.showSuccess("Bạn xoá dữ liệu thành công");
                            this.dataSource.items = this.dataSource.items.filter(d => d.id !== course.id)
                        }, () => this.showError("Bạn xoá dữ liệu thất bại"))
                    }                
                }
            })
        }
       
    }

    changeStatus(event, course: CourseDto) {
        const status = event.checked ? 1 : 0;
        this.CourseService.toggleStatus(course.id).subscribe(() => {
            course.status = status;
            this.showSuccess('Thay đổi trạng thái thành công!')
        }, error => {   
            event.checked = !event.checked;
            $('.switch_'+course.id).removeClass('mat-checked');
            if(event.checked) {
                $('.switch_'+course.id).addClass('mat-checked');
            }
        })
    }
}