import { Config } from '@abp/ng.core';

const baseUrl = 'http://localhost:4300';

export const environment = {
  production: true,
  application: {
    baseUrl,
    name: 'DanoneElearning',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://test-elearning-danone-api-advn.fractal.vn/',
    redirectUri: baseUrl,
    clientId: 'DanoneElearning_App',
    responseType: 'code',
    scope: 'offline_access DanoneElearning',
  },
  apis: {
    default: {
      url: 'https://test-elearning-danone-api-advn.fractal.vn/',
      rootNamespace: 'Ft.DanoneElearning',
    },
  },
} as Config.Environment;
