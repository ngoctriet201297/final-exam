import { AfterViewInit, ComponentFactoryResolver, ComponentRef, ContentChild, Directive, ElementRef, Injector, Renderer2, ViewContainerRef } from "@angular/core";
import { AbstractControl, NgControl } from "@angular/forms";
import { MatFormField, MatLabel } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";
import { FtMatErrorComponent } from "../../components/ft-mat-error/ft-mat-error.component";

@Directive({
    selector: 'mat-form-field'
})
export class ValidationDirective implements AfterViewInit {
    
    ref: ComponentRef<FtMatErrorComponent>;
    control: NgControl;
    @ContentChild(MatInput, { read: ElementRef}) controlElementRef: ElementRef;
    @ContentChild(MatInput, { read: AbstractControl}) abstractControl: AbstractControl;

    @ContentChild(MatLabel, {read: ViewContainerRef}) matLabel: ViewContainerRef;

    constructor(private readonly vcr: ViewContainerRef,
                private resolver: ComponentFactoryResolver,
                private readonly formField: MatFormField) {
    }
    defaultError = {
        minlength: (requiredLength: number) => `Chiều dài tối thiểu là ${requiredLength}`,
        maxlength: (requiredLength: number) => `Chiều dài tối đa là ${requiredLength}`,
        required: () => `Thuộc tính này là bắt buộc`,
        matDatepickerParse: () => "Định dạng thời gian không hợp lệ"
    }

    ngAfterViewInit(): void {
        this.control = this.formField._control.ngControl;
        if(this.controlElementRef){
            console.log(this.abstractControl)
            if(this.controlElementRef.nativeElement.hasAttribute('required')){
                console.log(this.matLabel)
            }

            this.controlElementRef.nativeElement.addEventListener("blur", () => this.onChange());
        }else if(this.control){
            this.control.statusChanges.subscribe(resp => this.onChange())
        }
    }

    onChange(){
        if(this.control.invalid && this.control.touched){
            let error = this.formField._control.placeholder + ' is incorrect';
            Object.keys(this.defaultError).forEach(k => {
                if(this.control.hasError(k)){
                    error = this.defaultError[k](this.control.errors[k]);
                }
            });
            this.setError(error);
        }else{
            this.setError("");
        }
    }

    setError(errorMessage: string){
        if(!this.ref){
            const factory = this.resolver.resolveComponentFactory(FtMatErrorComponent);
            this.ref = this.vcr.createComponent(factory);
        }
        this.ref.instance.error = errorMessage;
    }
}