﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Notifications.Dto
{
    public class CreateUpdateNotificationDto : BaseEntityDto<Guid?>
    {
        [MaxLength(NotificationConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(NotificationConsts.MaxLengthContent)]
        public string Content { get; set; }
        public List<CreateUpdateNotificationUserDto> NotificationUsers { get; set; }
    }
}
