﻿using System;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.Questions.Dto
{
    public class GetQuestionInputDto : PagedAndSortedResultRequestDto
    {
        public Guid? LessonId { get; set; }
    }
}
