﻿using System;

namespace Ft.Elearning.Admin.Questions.Dto
{
    public class QuestionDto : BaseEntityDto
    {
        public string Content { get; set; }
        public Guid LessonId { get; set; }
    }
}
