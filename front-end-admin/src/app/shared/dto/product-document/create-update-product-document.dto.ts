import { BaseEntityDto } from '../base-entity.dto';

export class CreateUpdateProductDocumentDto extends BaseEntityDto<string>{
    public name: string;
    public thumbnailId: string;
    public fileContentId: string;
    public description: string;
    public content: string;
}