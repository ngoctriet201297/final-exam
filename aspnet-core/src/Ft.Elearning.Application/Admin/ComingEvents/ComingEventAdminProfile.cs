﻿using System;
using AutoMapper;
using Ft.Elearning.Admin.ComingEvents.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.ComingEvents
{
    public class ComingEventAdminProfile : Profile
    {
        public ComingEventAdminProfile()
        {
            CreateMap<CreateUpdateComingEventDto, ComingEvent>();
            CreateMap<ComingEvent, GetComingEventForViewDto>();
            CreateMap<ComingEvent, GetComingEventForEditDto>();
        }
    }
}
