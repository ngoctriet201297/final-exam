﻿using AutoMapper;
using Ft.Elearning.Admin.Lessons.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Lessons
{
    public class LessonAdminProfile : Profile
    {
        public LessonAdminProfile()
        {
            CreateMap<CreateUpdateLessonDto, Lesson>();
            CreateMap<Lesson, GetLessonForViewDto>();
            CreateMap<Lesson, GetLessonForEditDto>();
        }
    }
}
