import { AfterContentInit, AfterViewInit, Component, Injector, Input, OnInit, ViewChild } from "@angular/core";
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR } from "@angular/forms";
import { AppBaseComponent } from "../../app-base-component";
import { AppHelper } from "../../helper/app-helper";
import { AppFileService } from "../../services/file/app-file.service";

declare var Quill: any;
@Component({
    templateUrl: "ft-editor.component.html",
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: FtEditorComponent,
            multi: true
        }
    ],
    selector: "ft-editor"
})
export class FtEditorComponent extends AppBaseComponent implements ControlValueAccessor, OnInit {

    editorId: string;
    editorControl = new FormControl("<h1>hello</h1>");
    onChange: (input: string) => void;
    private quill: any;
    @ViewChild("inputFile") inputFile: any;
    isProcessing: boolean;

    constructor(protected readonly injector: Injector,
        private readonly fileService: AppFileService ) {
        super(injector);
        this.editorId = "q" + AppHelper.generateGuid();
    }
    ngOnInit(): void {
        this.editorControl.valueChanges.subscribe(newValue => {
            this.onChange && this.onChange(newValue);
        });
        setTimeout(() => {
            this.quill = new Quill("#" + this.editorId, {
                theme: 'snow',
                modules: {
                    toolbar: {
                        container: [
                            ['bold', 'italic'],
                            [{'direction': 'rtl'}],
                            ['link', 'image'],
                            [{'align': []}],
                            [{'list':'ordered'},{'list':'bullet'}],
                            [{'header': [1,2,3,4,5,6, false]}],
                            [{'front':[]}],
                            [{size: ['small',false,'large','huge']}],
                            ['clean']
                        ],
                        handlers: {
                            image: () => {
                                this.inputFile.nativeElement.click();
                            }
                        }
                    } 
                }
            });
            this.quill.on("text-change", (delta, oldContents) => {
                this.editorControl.setValue(this.quill.container.firstChild.innerHTML);
            });
            this.quill.clipboard.dangerouslyPasteHTML(0, this.editorControl.value);
        }, 100);
    }

    writeValue(obj: any): void {
        this.editorControl.setValue(obj);
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {

    }
    setDisabledState?(isDisabled: boolean): void {
        
    }
    fileChange(event: any){
        let files = event.target.files;
        if(files.length){
            let formData = new FormData();
            formData.append("images", files[0]);
            this.isProcessing = true;
            this.fileService.insert(formData).subscribe((result) => {
                let fileUrl = this.getMediaById(result);
                let range = this.quill.getSelection();
                this.quill.insertEmbed(range.index, 'image', fileUrl);
                this.isProcessing = false;
            });
        }
    }
}

