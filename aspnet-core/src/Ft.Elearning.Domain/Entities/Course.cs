﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Course : BaseEntity
    {
        [MaxLength(CourseConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(CourseConsts.MaxLengthDescription)]
        public string Description { get; set; }
        public Guid ThumbnailId { get; set; }
        
        [ForeignKey("ThumbnailId")]
        public FileEntity Thumbnail { get; set; }
        public ICollection<Lesson> Lessons { get; set; }
    }
}