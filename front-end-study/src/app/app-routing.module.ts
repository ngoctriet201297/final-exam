import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guard/auth-guard';
import { FtApplicationLayoutComponent } from './shared/layout/ft-application-layout/ft-application-layout.component';

const routes: Routes = [
  {
    path: '',
    component: FtApplicationLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        loadChildren: () => import('./page/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'document',
        loadChildren: () => import('./page/document/document.module').then(m => m.DocumentModule)
      },
      {
        path: 'lesson',
        loadChildren: () => import('./page/lesson/lesson.module').then(m => m.LessonModule)
      },
      {
        path: 'event',
        loadChildren: () => import('./page/event/event.module').then(m => m.EventModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./page/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('./page/notification/notification.module').then(m => m.NotificationModule)
      },
      {
        path: 'test',
        loadChildren: () => import('./page/test/test.module').then(m => m.TestModule)
      },
    ]
  },
  {
    path: 'account',
    loadChildren: () => import('./page/account/account.module').then(m => m.AccountModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
