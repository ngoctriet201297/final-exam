using Ft.Elearning.MongoDB;
using Volo.Abp.Modularity;

namespace Ft.Elearning
{
    [DependsOn(
        typeof(ElearningMongoDbTestModule)
        )]
    public class ElearningDomainTestModule : AbpModule
    {

    }
}