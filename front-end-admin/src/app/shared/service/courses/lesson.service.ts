import { Injectable } from "@angular/core";
import { GetLessonInputDto, LessonDto } from '../../dto/lesson';
import { AppBaseService } from "../app-base-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
@Injectable({
    providedIn: "root"
})
export class LessonService extends AppBaseService<LessonDto, LessonDto, GetLessonInputDto, LessonDto> {
    prefix = environment.apis.default.url + "/api/app"
    constructor(protected readonly httpClient: HttpClient){
        super(httpClient,"lesson-admin");
    }

    toggleStatus(id: string) {
        return this.httpClient.post<null>( this.prefix + '/lesson-admin/' + id + '/toggle-status', null);
    }
}