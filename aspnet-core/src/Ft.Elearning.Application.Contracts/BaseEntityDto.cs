﻿using System;
using Ft.Elearning.Enums;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning
{
    public class BaseEntityDto<T> : EntityDto<T>
    {
        public Status Status { get; set; }
        public int DistributorId { get; set; }
    }
    public class BaseEntityDto : BaseEntityDto<Guid>
    {

    }
}
