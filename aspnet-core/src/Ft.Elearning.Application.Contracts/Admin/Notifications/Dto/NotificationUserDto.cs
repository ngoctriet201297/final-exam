﻿using System;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.Notifications.Dto
{
    public class NotificationUserDto: EntityDto<Guid>
    {
        public Guid NotificationId { get; set; }
        public int UserId { get; set; }
    }
}