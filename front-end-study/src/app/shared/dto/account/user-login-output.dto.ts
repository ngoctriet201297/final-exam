export class UserLoginOutputDto{
    token: string;
    fullName: string;
    expiredAt: string | Date;
}
