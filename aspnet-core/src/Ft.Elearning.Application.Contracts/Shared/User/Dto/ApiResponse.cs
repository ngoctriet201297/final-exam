﻿using System;

namespace Ft.Elearning.Shared.User.Dto
{
    [Serializable]
    public class ApiResponse<T>
    {
        public int code { get; set; }
        public int data_summary { get; set; }
        public T data { get; set; }
        public string message { get; set; }
    }
}
