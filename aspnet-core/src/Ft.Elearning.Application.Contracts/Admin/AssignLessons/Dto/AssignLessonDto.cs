﻿using System;
using System.Collections.Generic;

namespace Ft.Elearning.Admin.AssignLessons.Dto
{
    public class AssignLessonDto : BaseEntityDto
    {
        public Guid LessonId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        
        public List<CreateUpdateAssignLessonUserDto> AssignUsers { get; set; }
    }
}
