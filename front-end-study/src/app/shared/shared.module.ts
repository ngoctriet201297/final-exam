import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { NgxValidateCoreModule } from '@ngx-validate/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FtApplicationLayoutComponent } from './layout/ft-application-layout/ft-application-layout.component';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { FtLayoutHeaderComponent } from './layout/ft-layout-header/ft-layout-header.component';
import { FtSidenavComponent } from './layout/ft-sidenav/ft-sidenav.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FtUploadImageComponent } from './components/ft-upload-image-component/ft-upload-image.component';
import { FtUploadFileComponent } from './components/ft-upload-file-component/ft-upload-file.component';
import { FtEditorComponent } from './components/ft-editor/ft-editor.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PopupComponent } from './components/popup-component/popup.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ErrorMessagePopupComponent } from './components/popup-component/message-component/error-message-popup.component';
import { InformationMessagePopupComponent } from './components/popup-component/message-component/information-message-popup.component';
import { WarningMessagePopupComponent } from './components/popup-component/message-component/warning-message-popup.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AnchorDirective } from './anchor-directive/anchor.directive';
import { ToastrModule } from 'ngx-toastr';
import { SafeHTMLPipe } from './pipe/safe-html-pipe/safe-html.pipe';
import { QuestionMessagePopupComponent } from './components/popup-component/message-component/question-message-popup.component';
@NgModule({
  declarations: [
    FtSidenavComponent,
    FtApplicationLayoutComponent,
    FtLayoutHeaderComponent,
    FtUploadImageComponent,
    FtUploadFileComponent,
    FtEditorComponent,
    PopupComponent,
    ErrorMessagePopupComponent,
    InformationMessagePopupComponent,
    QuestionMessagePopupComponent,
    WarningMessagePopupComponent,
    AnchorDirective,
    SafeHTMLPipe
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    FormsModule,
    NgbDropdownModule,
    NgxValidateCoreModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSnackBarModule,
    ToastrModule.forRoot(),
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    FormsModule,
    NgbDropdownModule,
    NgxValidateCoreModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    FtApplicationLayoutComponent,
    FtSidenavComponent,
    FtLayoutHeaderComponent,
    MatButtonModule,
    FtUploadImageComponent,
    FtUploadFileComponent,
    FtEditorComponent,
    PopupComponent,
    AnchorDirective,
    MatProgressBarModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    ToastrModule,
    SafeHTMLPipe
  ],
  providers: []
})
export class SharedModule { }
