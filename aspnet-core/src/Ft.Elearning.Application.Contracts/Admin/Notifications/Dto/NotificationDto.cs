﻿using System.Collections.Generic;

namespace Ft.Elearning.Admin.Notifications.Dto
{
    public class NotificationDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public List<CreateUpdateNotificationUserDto> NotificationUsers { get; set; }
    }
}
