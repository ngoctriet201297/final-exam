﻿using System;

namespace Ft.Elearning.Admin.ComingEvents.Dto
{
    public class ComingEventDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
