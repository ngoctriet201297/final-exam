﻿using Volo.Abp.Modularity;

namespace Ft.Elearning
{
    [DependsOn(
        typeof(ElearningApplicationModule),
        typeof(ElearningDomainTestModule)
        )]
    public class ElearningApplicationTestModule : AbpModule
    {

    }
}