﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Admin.Questions.Dto;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Lessons.Dto
{
    public class CreateUpdateLessonDto : BaseEntityDto<Guid?>
    {
        [MaxLength(LessonConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(LessonConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(LessonConsts.MaxLengthContent)]
        public string Content { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid CourseId { get; set; }
        public List<CreateUpdateQuestionDto> Questions { get; set; }
    }
}
