﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ft.Elearning.Admin.AssignTests.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.AssignTests
{
    public class AssignTestAdminAppService : AppBaseAdminService<AssignTest, Guid, CreateUpdateAssignTestDto, GetAssignTestForViewDto, GetAssignTestForEditDto, GetAssignTestInputDto>, IAssignTestAdminAppService
    {
            private readonly IRepository<AssignTest, Guid> _assignTestRepository;
            private readonly IRepository<AssignTestUser, Guid?> _assignTestUserRepository;
            
        public AssignTestAdminAppService(IRepository<AssignTest, Guid> assignTestRepository, IRepository<AssignTestUser, Guid?> assignTestUserRepository) : base(assignTestRepository)
        {
            _assignTestRepository = assignTestRepository;
            _assignTestUserRepository = assignTestUserRepository;
        }

        public override Task<Guid> Save(CreateUpdateAssignTestDto input)
        {
            var isAssignToAll = input.IsAssignToAll;
            var isManagerByAll = input.IsManageByAll;

            if (isAssignToAll)
            {
                input.AssignTestUser = input.AssignTestUser.Where(x => x.IsManager).ToList();
            }

            if (isManagerByAll)
            {
                input.AssignTestUser = input.AssignTestUser.Where(x => !x.IsManager).ToList();
            }

            return base.Save(input);
        }

        public override async Task Delete(Guid id)
        {
            var assignTest = await _assignTestRepository.FirstOrDefaultAsync(x => x.Id == id);
            if (assignTest == null)
            {
                throw new Exception("Item not found");
            }
            
            await _assignTestUserRepository.DeleteAsync(x => x.AssignTestId == assignTest.Id);
            await _assignTestRepository.DeleteAsync(assignTest);
            await CurrentUnitOfWork.SaveChangesAsync();
        }
    }
}
