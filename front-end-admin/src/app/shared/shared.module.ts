import { NgModule } from '@angular/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { FtApplicationLayoutComponent } from './layout/ft-application-layout/ft-application-layout.component';
import { FtLayoutHeaderComponent } from './layout/ft-layout-header/ft-layout-header.component';
import { FtSidenavComponent } from './layout/ft-sidenav/ft-sidenav.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FtUploadImageComponent } from './components/ft-upload-image-component/ft-upload-image.component';
import { FtUploadFileComponent } from './components/ft-upload-file-component/ft-upload-file.component';
import { FtEditorComponent } from './components/ft-editor/ft-editor.component';
import { CommonModule } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ToastrModule } from 'ngx-toastr';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PopupComponent } from './components/popup-component/popup.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AnchorDirective } from './directive/anchor-directive/anchor.directive';
import { ErrorMessagePopupComponent } from './components/popup-component/message-component/error-message-popup.component';
import { InformationMessagePopupComponent } from './components/popup-component/message-component/information-message-popup.component';
import { WarningMessagePopupComponent } from './components/popup-component/message-component/warning-message-popup.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SafeHTMLPipe } from './pipe/safe-html-pipe/safe-html.pipe';
import { FtMatErrorComponent } from './components/ft-mat-error/ft-mat-error.component';
import { QuestionMessagePopupComponent } from './components/popup-component/message-component/question-message-popup.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { ValidationDirective } from './directive/validation-directive/validation.directive';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
@NgModule({
  declarations: [
    FtSidenavComponent,
    FtApplicationLayoutComponent,
    FtLayoutHeaderComponent,
    FtUploadImageComponent,
    FtUploadFileComponent,
    FtEditorComponent,
    PopupComponent,
    ErrorMessagePopupComponent,
    InformationMessagePopupComponent,
    QuestionMessagePopupComponent,
    WarningMessagePopupComponent,
    AnchorDirective,
    SafeHTMLPipe,
    FtMatErrorComponent,
    ValidationDirective
  ],
  imports: [
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    CommonModule,
    MatProgressBarModule,
    MatSnackBarModule,
    ToastrModule.forRoot(),
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
  ],
  exports: [
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    FtApplicationLayoutComponent,
    FtSidenavComponent,
    FtLayoutHeaderComponent,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    FtUploadImageComponent,
    FtUploadFileComponent,
    FtEditorComponent,
    CommonModule,
    MatProgressBarModule,
    MatSnackBarModule,
    ToastrModule,
    MatPaginatorModule,
    PopupComponent,
    MatDialogModule,
    AnchorDirective,
    MatProgressSpinnerModule,
    SafeHTMLPipe,
    FtMatErrorComponent,
    ValidationDirective,
    MatSlideToggleModule
  ],
  providers: [
  
  ]
})
export class SharedModule { }
