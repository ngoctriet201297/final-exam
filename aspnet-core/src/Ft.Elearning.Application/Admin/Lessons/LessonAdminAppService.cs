﻿using System;
using System.Threading.Tasks;
using Ft.Elearning.Admin.Lessons.Dto;
using Ft.Elearning.Entities;
using Volo.Abp.Domain.Repositories;

namespace Ft.Elearning.Admin.Lessons
{
    public class LessonAdminAppService : AppBaseAdminService<Lesson, Guid, CreateUpdateLessonDto, GetLessonForViewDto, GetLessonForEditDto, GetLessonInputDto>,
                                                  ILessonAdminAppService
    {
        public LessonAdminAppService(IRepository<Lesson, Guid> repository) : base(repository)
        {
        }

        protected override Task<Guid> Update(CreateUpdateLessonDto input)
        {
            input.Questions = null;
            return base.Update(input);
        }
    }
}
