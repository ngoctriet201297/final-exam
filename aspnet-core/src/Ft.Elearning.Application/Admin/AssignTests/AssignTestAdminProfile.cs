﻿using AutoMapper;
using Ft.Elearning.Admin.AssignTests.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.AssignTests
{
    public class AssignTestAdminProfile : Profile
    {
        public AssignTestAdminProfile()
        {
            CreateMap<CreateUpdateAssignTestDto, AssignTest>();
            CreateMap<CreateUpdateAssignTestUserDto, AssignTestUser>();
            CreateMap<AssignTest, GetAssignTestForViewDto>();
            CreateMap<AssignTest, GetAssignTestForEditDto>();
        }
    }
}
