﻿using MongoDB.Driver;
using Ft.Elearning.Users;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;
using Ft.Elearning.Entities;

namespace Ft.Elearning.MongoDB
{
    [ConnectionStringName("Default")]
    public class ElearningMongoDbContext : AbpMongoDbContext
    {
        public IMongoCollection<AppUser> Users => Collection<AppUser>();
        public IMongoCollection<FileEntity> FileEntities => Collection<FileEntity>();
        public IMongoCollection<Notification> Notifications => Collection<Notification>();
        public IMongoCollection<NotificationUser> NotificationsUsers => Collection<NotificationUser>();
        public IMongoCollection<ComingEvent> ComingEvents => Collection<ComingEvent>();
        public IMongoCollection<Document> Documents => Collection<Document>();
        public IMongoCollection<Course> Courses => Collection<Course>();
        public IMongoCollection<Lesson> Lessons => Collection<Lesson>();
        public IMongoCollection<Test> Tests => Collection<Test>();
        public IMongoCollection<Question> Questions => Collection<Question>();
        public IMongoCollection<TestQuestion> TestQuestions => Collection<TestQuestion>();
        public IMongoCollection<Answer> Answers => Collection<Answer>();
        public IMongoCollection<AssignLesson> AssignLessons => Collection<AssignLesson>();
        public IMongoCollection<AssignTest> AssignTests => Collection<AssignTest>();
        public IMongoCollection<AssignLessonUser> AssignLessonUsers => Collection<AssignLessonUser>();
        public IMongoCollection<AssignTestUser> AssignTestUsers => Collection<AssignTestUser>();

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.Entity<AppUser>(b =>
            {
                /* Sharing the same "AbpUsers" collection
                 * with the Identity module's IdentityUser class. */
                b.CollectionName = "AbpUsers";
            });

            modelBuilder.Entity<FileEntity>(b =>
            {
                /* Sharing the same "AbpUsers" collection
                 * with the Identity module's IdentityUser class. */
                b.CollectionName = "FileEntities";
            });
        }
    }
}
