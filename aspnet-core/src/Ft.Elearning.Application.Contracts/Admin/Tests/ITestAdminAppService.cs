﻿using System;
using Ft.Elearning.Admin.Tests.Dto;

namespace Ft.Elearning.Admin.Tests
{
    public interface ITestAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateTestDto, GetTestForViewDto, GetTestForEditDto, GetTestInputDto>
    {

    }
}
