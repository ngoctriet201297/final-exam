﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Test : BaseEntity
    {
        [MaxLength(TestConsts.MaxLengthName)]
        public string Name { get; set; }
        [MaxLength(TestConsts.MaxLengthTitle)]
        public string Title { get; set; }
        [MaxLength(TestConsts.MaxLengthDescription)]
        public string Description { get; set; }
        public int PercentFinished { get; set; }
        public int TimeTotal { get; set; }
        public DateTime? EffectiveDateFrom { get; set; }
        public DateTime? EffectiveDateTo { get; set; }
        public Guid ThumbnailId { get; set; }
        
        [ForeignKey("ThumbnailId")]
        public FileEntity Thumbnail { get; set; }
        
        public ICollection<TestQuestion> TestQuestions { get; set; }
    }
}