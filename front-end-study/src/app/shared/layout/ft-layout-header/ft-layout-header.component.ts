import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { AppBaseComponent } from '../../app-base-component';
import { AccountService } from '../../services/account/account.service';

@Component({
    templateUrl: 'ft-layout-header.component.html',
    selector: 'ft-layout-header'
})
export class FtLayoutHeaderComponent extends AppBaseComponent{
    constructor(protected readonly injector: Injector,
                private readonly accountService: AccountService){
        super(injector);
    }

    @Output()
    menuClick: EventEmitter<any> = new EventEmitter();
    @Input()
    showBtn: boolean;

    menuBtnClick(){
        this.menuClick.emit();
    }

    logout() {
        this.openPopup({
            message: 'Đăng xuất tài khoản?',
            title: 'Xác nhận',
            btnClose: {
                callBack: () => {

                },
                label: 'Hủy'
            },
            btnOk: {
                callBack: () => {
                    this.accountService.logout();
                    this.redirectTo('/account/login');
                },
                label: 'Đăng xuất'
            },
            type: 'Question'
        });
    }
}
