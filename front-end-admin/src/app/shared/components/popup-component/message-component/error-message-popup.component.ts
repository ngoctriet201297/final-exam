import { Component, Input } from "@angular/core";

@Component({
    template: `
        <div class="text-danger d-flex" mat-dialog-title><mat-icon>error</mat-icon> {{title}}</div>
        <div class="text-center message" mat-dialog-content>
            {{message}}
        </div>`
})
export class ErrorMessagePopupComponent {
    @Input() message: string;
    @Input() title: string;
}
