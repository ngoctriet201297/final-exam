﻿namespace Ft.Elearning.Consts
{
    public class NotificationConsts
    {
        public const int MaxLengthName = 255;
        public const int MaxLengthContent = 5000;
    }
}