﻿using System;
using AutoMapper;
using Ft.Elearning.Admin.Documents.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Documents
{
    public class DocumentAdminProfile : Profile
    {
        public DocumentAdminProfile()
        {
            CreateMap<CreateUpdateDocumentDto, Document>();
            CreateMap<Document, GetDocumentForViewDto>();
            CreateMap<Document, GetDocumentForEditDto>();
        }
    }
}
