﻿using System;
using System.Collections.Generic;

namespace Ft.Elearning.Admin.AssignTests.Dto
{
    public class AssignTestDto : BaseEntityDto
    {
        public Guid TestId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        
        public List<CreateUpdateAssignTestUserDto> AssignTestUser { get; set; }
    }
}
