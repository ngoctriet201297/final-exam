﻿using System;
using Volo.Abp.Application.Dtos;

namespace Ft.Elearning.Admin.AssignLessons.Dto
{
    public class CreateUpdateAssignLessonUserDto : EntityDto<Guid?>
    {
        public Guid AssignLessonId { get; set; }
        public int UserId { get; set; }
        public bool IsManager { get; set; }
    }
}