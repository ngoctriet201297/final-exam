import { Component } from "@angular/core";
import { SpinnerService } from "../../service/spinner/spinner.service";

@Component({
    templateUrl: "ft-background-spinner.component.html",
    selector: "app-spinner"
})
export class FtBackgroundSpinnerComponent{
    constructor(public readonly spinnerService: SpinnerService){

    }
}