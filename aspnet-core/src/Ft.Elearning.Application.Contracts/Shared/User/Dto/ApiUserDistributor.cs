﻿using System;

namespace Ft.Elearning.Shared.User.Dto
{
    [Serializable]
    public class ApiUserDistributor
    {
        public int user_id { get; set; }
        public string user_name { get; set; }
        public string user_full_name { get; set; }
        public string user_phone { get; set; }
        public int user_distributor_id { get; set; }
        public int distributor_id { get; set; }
        public string distributor_name { get; set; }
    }
}
