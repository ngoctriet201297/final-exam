﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ft.Elearning.Shared.User.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Volo.Abp;

namespace Ft.Elearning.Shared.User
{
    public class UserService : ElearningAppService, IUserService
    {
        private readonly IConfiguration _configuration;

        public UserService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [RemoteService(false)]
        public async Task<ApiResponse<ApiLoginDataDto>> Login(ApiLoginRequest request)
        {
            return new ApiResponse<ApiLoginDataDto>();
        }


    }
}
