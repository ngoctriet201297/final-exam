﻿using System;
using Ft.Elearning.Admin.Lessons.Dto;

namespace Ft.Elearning.Admin.Lessons
{
    public interface ILessonAdminAppService :
        IAppBaseAdminService<Guid, CreateUpdateLessonDto, GetLessonForViewDto, GetLessonForEditDto, GetLessonInputDto>
    {

    }
}
