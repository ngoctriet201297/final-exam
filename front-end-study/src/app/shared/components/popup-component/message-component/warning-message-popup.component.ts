import { Component, Input } from "@angular/core";

@Component({
    template: `<div class="d-flex text-warning" mat-dialog-title><mat-icon>warning</mat-icon> {{title}}</div>
    <div class="text-center message" mat-dialog-content>
    {{message}}
    </div>`
})
export class WarningMessagePopupComponent {
    @Input() message: string;
    @Input() title: string;
}