﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ft.Elearning.Admin.AssignTests.Dto
{
    public class CreateUpdateAssignTestDto : BaseEntityDto<Guid?>
    {
        [Required]
        public Guid TestId { get; set; }
        public bool IsAssignToAll { get; set; }
        public bool IsManageByAll { get; set; }
        [Required]
        public DateTime? From { get; set; }
        [Required]
        public DateTime? To { get; set; }
        
        public List<CreateUpdateAssignTestUserDto> AssignTestUser { get; set; }
    }
}
