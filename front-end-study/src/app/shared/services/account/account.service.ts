import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLoginInputDto } from '../../dto/account/user-login-input.dto';
import { UserLoginOutputDto } from '../../dto/account/user-login-output.dto';
@Injectable({
    providedIn: 'root'
})
export class AccountService {
    constructor(private readonly httpClient: HttpClient) {
    }

    login(user: UserLoginInputDto, callBack: () => void) {
        return this.httpClient.post('/login', user).subscribe((result: UserLoginOutputDto) => {
            if (result) {
                localStorage.setItem('token', result.token);
                localStorage.setItem('token_expire', new Date(result.expiredAt).getTime().toString());
                localStorage.setItem('user_fullname', result.fullName);

                if (callBack) {
                    callBack();
                }
            }
        }, (error) => {
            console.log(error);
        });
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('token_expire');
        localStorage.removeItem('user_fullname');
    }
}
