﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Ft.Elearning.Entities
{
    public class NotificationUser : Entity<Guid?>
    {
        public Guid NotificationId { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        
        [ForeignKey("NotificationId")]
        public Notification Notification { get; set; }
    }
}