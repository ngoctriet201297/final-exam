﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Entities
{
    public class Document : BaseEntity
    {
        [MaxLength(DocumentConsts.MaxLengthName)]
        public string Name { get; set; }
        public Guid ThumbnailId { get; set; }
        public Guid FileContentId { get; set; }
        [MaxLength(DocumentConsts.MaxLengthDescription)]
        public string Description { get; set; }
        [MaxLength(DocumentConsts.MaxLengthContent)]
        public string Content { get; set; }
        
        [ForeignKey("ThumbnailId")]
        public FileEntity Thumbnail { get; set; }

        [ForeignKey("FileContentId")]
        public FileEntity FileContent { get; set; }
    }
}