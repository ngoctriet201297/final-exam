﻿using System;
using System.Linq;

namespace Ft.Elearning.Helper
{
    public class GenerateHelper
    {
        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var charsLower = chars.ToLower();
            const string charsNumber = "1234567890";
            const string charsSpecialSign = "!@#$%^&*()";

            var result = new string(Enumerable.Repeat(chars, 1)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            result += new string(Enumerable.Repeat(charsLower, length - 3)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            result += new string(Enumerable.Repeat(charsNumber, 1)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            result += new string(Enumerable.Repeat(charsSpecialSign, 1)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return result;
        }

        public static string GeneratePassword()
        {
            return RandomString(8);
        }

        public static string GenerateActiveCode()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var result = new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
    }
}
