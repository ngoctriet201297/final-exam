﻿using System;

namespace Ft.Elearning.Shared.User.Dto
{
    [Serializable]
    public class ApiProfileDto
    {
        public int user_id { get; set; }
        public string company_id { get; set; }
        public string user_name { get; set; }
        public string user_is_first_password_change { get; set; }
        public string user_full_name { get; set; }
        public object region_id { get; set; }
        public string province_id { get; set; }
        public object district_id { get; set; }
        public string user_phone { get; set; }
        public string channel_id { get; set; }
        public string permission_id { get; set; }
        public string user_is_admin { get; set; }
        public string user_is_active { get; set; }
        public string user_is_manager { get; set; }
        public string hcp_active { get; set; }
        public string elearning_active { get; set; }
        public string role_id { get; set; }
        public int distributor_id { get; set; }
        public string distributor_name { get; set; }
        public object ca_permission_is_sup { get; set; }
    }
}
