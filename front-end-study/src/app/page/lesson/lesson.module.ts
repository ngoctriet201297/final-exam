import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { LessonRoutingModule } from './lesson-routing.module';
import { LessonComponent } from './lesson.component';

@NgModule({
  declarations: [LessonComponent],
  imports: [SharedModule, LessonRoutingModule],
})
export class LessonModule {}
