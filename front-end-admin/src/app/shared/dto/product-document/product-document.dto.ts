import { BaseEntityDto } from '../base-entity.dto';

export class ProductDocumentDto extends BaseEntityDto<string>{
    public name: string;
    public thumbnailId: string;
    public fileContentId: string;
    public description: string;
    public content: string;
}