import { Component, Injector, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { ApiUserDistributor } from "src/app/shared/dto/api-user/api-user-distributor";
import { CourseService } from "src/app/shared/service/courses/course.service";
import { UserService } from "src/app/shared/service/user/user-service";

@Component({
    templateUrl: "./create-edit-course.component.html"
})
export class CreateEditCourseComponent extends AppBaseComponent implements OnInit{
    courseForm: FormGroup;
    distributors: ApiUserDistributor[];
    get courseId(): string{
        return this.getParam("id");
    }

    constructor(protected readonly injector: Injector,
                private readonly fb: FormBuilder,
                private readonly courseService: CourseService,
                private readonly userService: UserService){
        super(injector)

        this.courseForm = fb.group({
            name: ["",[Validators.required, Validators.maxLength(255)]],
            thumbnailId: [""],
            description: [""],
            distributorId: ["",[Validators.required]]
        });
    }

    get name(){
        return this.courseForm.get("name");
    }

    ngOnInit(): void {
        this.getInitData();
        if(this.courseId){
            this.courseService.get(this.courseId).subscribe((result) => {
                this.courseForm.patchValue({
                    ...result
                })
            })
        }
    }

    save(){
        if(this.courseForm.valid){
            this.setBusy();
            const body = this.courseId ? {...this.courseForm.value, ...{ id: this.courseId } } : this.courseForm.value;
            this.courseService.save(body).subscribe(() => {
                this.showSuccess("Bạn đã cập nhật thành công");
                this.goBack();
            },() => {
                this.showError("Cập nhật thất bại");
            })
        }
    }

    getInitData(){
        // this.userService.getDistributor().subscribe(result => {
        //     this.distributors = result.data;
        // });
    }

    goBack(){
        this.redirectTo("/admin/courses/course")
    }
}