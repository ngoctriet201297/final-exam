﻿namespace Ft.Elearning.Consts
{
    public class ComingEventConsts
    {
        public const int MaxLengthName = 255;
        public const int MaxLengthContent = 5000;
    }
}