import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AppBaseComponent } from '../app-base-component';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard extends AppBaseComponent implements CanActivate{

    constructor(protected readonly injector: Injector){
        super(injector);
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if (this.tokenInfo.token) {
            return true;
        }
        this.redirectTo('/account/login');
    }
}
