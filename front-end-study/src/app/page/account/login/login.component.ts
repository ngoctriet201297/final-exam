import { Component, Injector, OnInit } from '@angular/core';
import { AppBaseComponent } from 'src/app/shared/app-base-component';
import { UserLoginInputDto } from 'src/app/shared/dto/account/user-login-input.dto';
import { AccountService } from 'src/app/shared/services/account/account.service';
import { environment } from 'src/environments/environment';

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent extends AppBaseComponent implements OnInit {
    vm: UserLoginInputDto = environment.production ? {} : {
        username: 'phong',
        password: 'admin@123'
    };

    constructor(protected readonly injector: Injector,
                private readonly accountService: AccountService) {
        super(injector);
    }
    ngOnInit(): void {
        if (this.tokenInfo.token) {
            this.redirectTo('/home');
        }
    }
    login() {
        this.setBusy();
        this.accountService.login(this.vm, () => {
            this.redirectTo('/');
        });
    }
}
