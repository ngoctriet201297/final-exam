﻿using System;
using System.ComponentModel.DataAnnotations;
using Ft.Elearning.Consts;

namespace Ft.Elearning.Admin.Answers.Dto
{
    public class CreateUpdateAnswerDto : BaseEntityDto<Guid?>
    {
        [Required]
        [MaxLength(AnswerConsts.MaxLengthContent)]
        public string Content { get; set; }
        public bool IsRightAnswer { get; set; }
        [Required]
        public Guid QuestionId { get; set; }
    }
}
