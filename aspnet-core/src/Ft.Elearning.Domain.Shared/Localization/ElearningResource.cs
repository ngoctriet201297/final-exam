﻿using Volo.Abp.Localization;

namespace Ft.Elearning.Localization
{
    [LocalizationResourceName("Elearning")]
    public class ElearningResource
    {

    }
}