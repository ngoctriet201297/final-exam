﻿using AutoMapper;
using Ft.Elearning.Admin.Notifications.Dto;
using Ft.Elearning.Entities;

namespace Ft.Elearning.Admin.Notifications
{
    public class NotificationAdminProfile : Profile
    {
        public NotificationAdminProfile()
        {
            CreateMap<CreateUpdateNotificationDto, Notification>();
            CreateMap<CreateUpdateNotificationUserDto, NotificationUser>();
            CreateMap<Notification, GetNotificationForViewDto>();
            CreateMap<Notification, GetNotificationForEditDto>();
        }
    }
}
